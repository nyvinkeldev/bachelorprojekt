var PI = Math.PI
var PI2 = 2 * Math.PI
var cos = Math.cos
var sin = Math.sin

function vec(x, y, z) {
	return [x, y, z]
}

function scaleVec(s, v) {
	return [s * v[0], s * v[1], s * v[2]]
}

function sphereCoords(theta, phi) {
	return vec(
		cos(theta) * sin(phi),
		sin(theta) * sin(phi),
		cos(phi)
	)
}

function line(from, to) {
	return [from, to]
}

function mat(a11, a12, a13,
			 a21, a22, a23,
			 a31, a32, a33) {
	return [a11, a12, a13, a21, a22, a23, a31, a32, a33]
}

function matRotX(theta) {
	return mat(
		1, 0, 0,
		0, cos(theta), -sin(theta),
		0, sin(theta), cos(theta)
	)
}

function matRotY(theta) {
	return mat(
		cos(theta), 0, -sin(theta),
		0, 1, 0,
		sin(theta), 0, cos(theta)
	)
}

function matRotZ(theta) {
	return mat(
		cos(theta), -sin(theta), 0,
		sin(theta), cos(theta), 0,
		0, 0, 1
	)
}

function mulMatVec(m, v) {
	var [a11, a12, a13, a21, a22, a23, a31, a32, a33] = m
	var [v1, v2, v3] = v
	
	return vec(
		a11 * v1 + a12 * v2 + a13 * v3,
		a21 * v1 + a22 * v2 + a23 * v3,
		a31 * v1 + a32 * v2 + a33 * v3
	)
}

function mulMatMat(a, b) {
	var [a11, a12, a13, a21, a22, a23, a31, a32, a33] = a
	var [b11, b12, b13, b21, b22, b23, b31, b32, b33] = b

	return mat(
		a11 * b11 + a12 * b21 + a13 * b31,
		a11 * b12 + a12 * b22 + a13 * b32,
		a11 * b13 + a12 * b23 + a13 * b33,
		a21 * b11 + a22 * b21 + a23 * b31,
		a21 * b12 + a22 * b22 + a23 * b32,
		a21 * b13 + a22 * b23 + a23 * b33,
		a31 * b11 + a32 * b21 + a33 * b31,
		a31 * b12 + a32 * b22 + a33 * b32,
		a31 * b13 + a32 * b23 + a33 * b33
	)
}

function rollMat(phi, theta) {
	return mulMatMat(matRotY(phi), mulMatMat(matRotZ(theta), matRotY(-phi)))
}

function transformLineList(lineList, mat) {
	for (var line of lineList) {
		line[0] = mulMatVec(mat, line[0])
		line[1] = mulMatVec(mat, line[1])
	}
}

function makeSphere(radius) {
	var lines = []
	var resH = 16
    var resV = 24
    
    for (var i = 0; i < resH; i++) {
    	for (var j = 0; j < resV; j++) {
    		var point      = scaleVec(radius, sphereCoords(PI2 / resV * j, PI / resH * i))
    		var pointRight = scaleVec(radius, sphereCoords(PI2 / resV * (j + 1), PI / resH * i))
    		var pointDown  = scaleVec(radius, sphereCoords(PI2 / resV * j, PI / resH * (i + 1)))
    		var pointDiag  = scaleVec(radius, sphereCoords(PI2 / resV * (j + 1), PI / resH * (i + 1)))
    		
    		/*
				point ------ pointRight
				  |           -  |
				  |        -     |
				  |     -        |
				  |  -           |
				pointDown -- pointDiag
    		*/

    		lines.push(line(point, pointRight))
    		lines.push(line(point, pointDown))
    	}
    }

    return lines
}

var prog = `\\begin{tikzpicture}\n`
var res = 64
var rad = 3
var sphere = makeSphere(3)

transformLineList(sphere, rollMat(Math.PI/3, 2 * Math.PI/3))

for (var i = 0; i < sphere.length; i++) {
	var [from, to] = sphere[i]
	var [fromX, fromY, fromZ] = from
	var [toX, toY, toZ] = to

	if (fromX < 0 || toX < 0)
		prog += `\t\\draw (${fromY},${fromZ}) -- (${toY},${toZ});\n`
}

prog += `\\end{tikzpicture}`

console.log(prog)