/*
	Model:
		- Array of vertices [(x, y, z)]
		- Array of triangles [(id1, id2, id3)] - id{i} is a index into the array of vertices
*/

function vec3(x, y, z) {
	return [x, y, z]
}

function vec2(x, y) {
	return [x, y]
}

function subvec2(u, v) {
	return vec2(u[0] - v[0], u[1] - v[1])
}

function subvec3(u, v) {
	return vec3(u[0] - v[0], u[1] - v[1], u[2] - v[2])
}

function addvec3(u, v) {
	return vec3(u[0] + v[0], u[1] + v[1], u[2] + v[2])
}

function scale3(s, v) {
	return [s * v[0], s * v[1], s * v[2]]
}

function cross(v, w) {
	return vec3(
		v[1] * w[2] - v[2] * w[1],
		v[2] * w[0] - v[0] * w[2],
		v[0] * w[1] - v[1] * w[0]
	)
}

function dot(v, w) {
	return v[0] * w[0] + v[1] * w[1] + v[2] * w[2]
}

function norm(v) {
	return Math.sqrt(dot(v, v))
}

function project(v, w) {
	return scale3(dot(v, w) / dot(w, w), w)
}

function det2(u, v) {
	return u[0] * v[1] - u[1] * v[0]
}

var cube = {
	verts: [
		vec3(-1.0, -1.0, -1.0),
		vec3( 1.0, -1.0, -1.0),
		vec3( 1.0,  1.0, -1.0),
		vec3(-1.0,  1.0, -1.0),
		vec3(-1.0, -1.0,  1.0),
		vec3( 1.0, -1.0,  1.0),
		vec3( 1.0,  1.0,  1.0),
		vec3(-1.0,  1.0,  1.0)
	],
	tris: [
		0, 1, 3, // front
		1, 2, 3,
		4, 7, 5, // back
		7, 6, 5,
		7, 4, 0, // left
		3, 7, 0,
		1, 5, 6, // right
		1, 6, 2,
		5, 1, 0, // top
		4, 5, 0,
		3, 2, 6, // bottom
		3, 6, 7
	]
}

var sphere = (function() {
	var resV = 64
	var resH = 128

	// theta is in [0, pi], phi is in [0, 2pi]
	function sphereCoords(theta, phi) {
		return vec3(
			Math.sin(theta) * Math.cos(phi),
			Math.sin(theta) * Math.sin(phi),
			Math.cos(theta)
		)
	}

	var model = { verts: [], tris: [] }
	var vertDict = {}

    function vertDictKey(i, j) {
    	if (i == 0 || i == resV) j = 0
    	return `${i}:${j % resH}`
    }

	function vertDictGetIndex(i, j) {
		var key = vertDictKey(i, j)

		if (!vertDict.hasOwnProperty(key)) {
			var index = model.verts.length
			vertDict[key] = index
			model.verts.push(sphereVert(i, j))
		}

		return vertDict[key]
	}

	function sphereVert(i, j) {
		return sphereCoords(Math.PI / resV * i, 2 * Math.PI / resH * j)
	}

	for (var i = 0; i < resV; i++) {
		for (var j = 0; j < resH; j++) {
			var point      = vertDictGetIndex(i, j)
    		var pointRight = vertDictGetIndex(i, j + 1)
    		var pointDown  = vertDictGetIndex(i + 1, j)
    		var pointDiag  = vertDictGetIndex(i + 1, j + 1)
    		
    		//
			//	point ------ pointRight
			//	  |           -  |
			//	  |        -     |
			//	  |     -        |
			//	  |  -           |
			//	pointDown -- pointDiag
    		//

    		if (i > 0)        model.tris.push(point, pointRight, pointDown)
    		if (i < resV - 1) model.tris.push(pointDown, pointRight, pointDiag)
		}
	}

	return model
})()

var torus = (function() {
	var RING_CIRC_RESOLUTION = 64//64
	var RING_EDGE_RESOLUTION = 64//32
	var RING_CIRC_RADIUS = 6.0
	var RING_EDGE_RADIUS = 2.0

	var model = { verts: [], tris: [] }

	var i, j
    for(i = 0; i < RING_CIRC_RESOLUTION; i++)
    {
        var circAngle = 2.0 * Math.PI / RING_CIRC_RESOLUTION * i
        var circCos = Math.cos(circAngle)
        var circSin = Math.sin(circAngle)
        
        for(j = 0; j < RING_EDGE_RESOLUTION; j++)
        {
            var edgeAngle = 2.0 * Math.PI / RING_EDGE_RESOLUTION * j;
            var edgeCos = Math.cos(edgeAngle);
            var edgeSin = Math.sin(edgeAngle);
            
            var xDist = RING_EDGE_RADIUS * circCos;
            var yDist = RING_EDGE_RADIUS * circSin;
            
            var domain = i * RING_EDGE_RESOLUTION;
            var nextDomain = (i + 1) * RING_EDGE_RESOLUTION;
            var offset = -1;
            
            if(i == RING_CIRC_RESOLUTION - 1)
                nextDomain = 0;
            
            if(j == 0)
                offset = RING_EDGE_RESOLUTION - 1;
            
            model.tris.push(
                domain + j + offset, nextDomain + j + offset, domain + j,
                nextDomain + j + offset, nextDomain + j, domain + j
            )
            
            model.verts.push(vec3(
                RING_CIRC_RADIUS * circCos + xDist * edgeCos,
                RING_CIRC_RADIUS * circSin + yDist * edgeCos,
                RING_EDGE_RADIUS * edgeSin
            ))
        }
    }

	return model
})()

var cylinder = (function() {
	var res = 64
	var layers = 5

	// theta is in [0, 2pi]
	function cylinderCoords(theta, h) {
		return vec3(
			h,
			Math.cos(theta),
			Math.sin(theta)
		)
	}

	var model = { verts: [], tris: [] }
	var vertDict = {}

    function vertDictKey(i, j) {
    	return `${i % res}:${j}`
    }

	function vertDictGetIndex(i, j) {
		var key = vertDictKey(i, j)

		if (!vertDict.hasOwnProperty(key)) {
			var index = model.verts.length
			vertDict[key] = index
			model.verts.push(cylinderVert(i, j))
		}

		return vertDict[key]
	}

	function cylinderVert(i, j) {
		return cylinderCoords(2 * Math.PI / res * i, j)
	}

	for (var i = 0; i < res; i++) {
		for (var j = -layers; j <= layers; j++) {
			var point      = vertDictGetIndex(i, j)
    		var pointRight = vertDictGetIndex(i, j + 1)
    		var pointDown  = vertDictGetIndex(i + 1, j)
    		var pointDiag  = vertDictGetIndex(i + 1, j + 1)

    		// same diagram as for sphere
    		
    		model.tris.push(point, pointRight, pointDown)
    		model.tris.push(pointDown, pointRight, pointDiag)
		}
	}

	return model
})()

function makeFunctionGraph(f) {
	var resW = 64
	var resH = 64
	var model = { verts: [], tris: [] }

	function coord(i, j) {
		return i * resH + j
	}

	for (var i = 0; i < resW; i++) {
		for (var j = 0; j < resH; j++) {
			var x = -1 + 2 * i / resW
			var y = -1 + 2 * j / resH
			
			model.verts[coord(i, j)] = vec3(x, f(x, y), y)

			var point      = coord(i, j)
    		var pointRight = coord(i, j + 1)
    		var pointDown  = coord(i + 1, j)
    		var pointDiag  = coord(i + 1, j + 1)

    		// same diagram as for sphere
    		
    		if(i < resW - 1 && j < resH - 1) {
    			model.tris.push(point, pointRight, pointDown)
    			model.tris.push(pointDown, pointRight, pointDiag)
    		}
		}
	}

	return model
}

function transform(model, f) {
	var newModel = {}

	newModel.tris = model.tris.slice()
	newModel.verts = model.verts.map(x => f(x))

	return newModel
}

function outline(model, projection) {
	var lines = {}
	var verts = model.verts.map(x => projection(x))
	var tris = model.tris

	function lineKey(id1, id2) {
		return Math.min(id1, id2) + ':' + Math.max(id1, id2)
	}

	function makeEntry(id1, id2) {
		var key = lineKey(id1, id2)
		if (!lines.hasOwnProperty(key)) {
			lines[key] = {from: id1, to: id2, vis: 0, count: 0}
		}
	}

	function increaseVisible(id1, id2) {
		makeEntry(id1, id2)
		lines[lineKey(id1, id2)].vis++
	}

	function increaseCount(id1, id2) {
		makeEntry(id1, id2)
		lines[lineKey(id1, id2)].count++
	}

	for (var i = 0; i < tris.length; i += 3) {
		var id1 = tris[i]
		var id2 = tris[i+1]
		var id3 = tris[i+2]
		var p1 = verts[id1]
		var p2 = verts[id2]
		var p3 = verts[id3]
		var lhs = subvec2(p2, p1)
		var rhs = subvec2(p3, p1)

		if (det2(lhs, rhs) < 0) {
			increaseVisible(id1, id2)
			increaseVisible(id2, id3)
			increaseVisible(id1, id3)
		}

		increaseCount(id1, id2)
		increaseCount(id2, id3)
		increaseCount(id1, id3)
	}

	var result = []

	for (var key in lines) {
		var line = lines[key]

		if (line.vis == 1 || line.count == 1) {
			var [lineX, lineY] = subvec2(verts[line.from], verts[line.to])
			var normSq = lineX * lineX + lineY * lineY

			result.push([verts[line.from], verts[line.to]])
		}
	}

	if (model.hasOwnProperty("contours")) {
		for (var i = 0; i < model.contours.length; i++) {
			var [v, w, front] = model.contours[i]
			v = projection(v)
			w = projection(w)

			var s = 0.4
			if (front || ((10000 + Math.min(v[0], w[0])) % s) <= s/2)
			//if (front) {
				result.push([v, w])
			/*} else {
				var d = 0.1
				var minX = Math.min(v[0], w[0])
				var maxX = Math.max(v[0], w[0])
				for (var i = minX; i < maxX; i += 2 * d) {

				}
			}*/
		}
	}

	return result
}

function addContours(model, heights) {
	model.contours = []

	function intersection(id1, id2, h) {
		var v = model.verts[id1]
		var w = model.verts[id2]
		var d1 = v[1] - h
		var d2 = w[1] - h

		if ((d1 <= 0 && d2 >= 0) || (d1 >= 0 && d2 <= 0)) {
			if (w[1] - v[1] == 0) {
				return v
			}

			var p = subvec3(w, v)
			var t = (h - v[1]) / (w[1] - v[1])
			
			return addvec3(v, scale3(t, p))
		}

		return null
	}

	for (var i = 0; i < model.tris.length; i += 3) {
		var id1 = model.tris[i]
		var id2 = model.tris[i+1]
		var id3 = model.tris[i+2]
		var p1 = model.verts[id1]
		var p2 = model.verts[id2]
		var p3 = model.verts[id3]
		var lhs = subvec2(p2, p1)
		var rhs = subvec2(p3, p1)

		for (var j = 0; j < heights.length; j++) {
			var i1 = intersection(id1, id2, heights[j])
			var i2 = intersection(id2, id3, heights[j])
			var i3 = intersection(id1, id3, heights[j])
			var points = []

			if (i1 != null) points.push(i1)
			if (i2 != null) points.push(i2)
			if (i3 != null) points.push(i3)

			if (points.length == 2) {
				model.contours.push([points[0], points[1], det2(lhs, rhs) < 0])
			}
		}
	}

	return model
}

function removePointsInHalfSpace(model, planePoint, planeNormal) {
	var newModel = { verts: [], tris: [] }

	function testPoint(p) {
		return dot(subvec3(p, planePoint), planeNormal) <= 0
	}

	newModel.verts = model.verts.slice()

	for (var i = 0; i < model.tris.length; i += 3) {
		var id1 = model.tris[i]
		var id2 = model.tris[i+1]
		var id3 = model.tris[i+2]
		var v1 = model.verts[id1]
		var v2 = model.verts[id2]
		var v3 = model.verts[id3]
		var w1 = subvec3(v1, planePoint)
		var w2 = subvec3(v2, planePoint)
		var w3 = subvec3(v3, planePoint)
		var dp1 = dot(subvec3(v1, planePoint), planeNormal)
		var dp2 = dot(subvec3(v2, planePoint), planeNormal)
		var dp3 = dot(subvec3(v3, planePoint), planeNormal)
		var count = 0

		if (dp1 <= 0) count++
		if (dp2 <= 0) count++
		if (dp3 <= 0) count++

		if (count == 3) {
			newModel.tris.push(id1, id2, id3)
		} else if (count == 2) {
			/*
				o
				|\
			    | \
			    |  \
				|   o
				|  /
			----------
				|/
				o
			*/
		} else if (count == 1) {
			/*
				o
				|\
			----------
			    |  \
				|   o
				|  /
				| /
				|/
				o
			*/
		}
	}

	return newModel
}

function makeCamera(eye, target, distToViewPlane) {
	var up = vec3(0, 1, 0)
	var basisOut = subvec3(target, eye)
	var basisUp = subvec3(up, project(up, basisOut))
	var basisRight = cross(basisOut, basisUp)

	basisOut = scale3(1 / norm(basisOut), basisOut)
	basisUp = scale3(1 / norm(basisUp), basisUp)
	basisRight = scale3(1 / norm(basisRight), basisRight)

	// (basisOut, basisUp, basisRight) is now an orthonormal basis for R^3

	var viewPlaneLocation = addvec3(eye, scale3(distToViewPlane, basisOut))
	
	return function(v) {
		// assumption: v and eye are an opposite sites of (basisRight, basisUp) plane

		v = subvec3(v, viewPlaneLocation)
		var e = subvec3(eye, viewPlaneLocation)

		var vO = vec3(dot(v, basisRight), dot(v, basisUp), dot(v, basisOut))
		var eO = vec3(dot(e, basisRight), dot(e, basisUp), dot(e, basisOut))
		var t = eO[2] / (eO[2] - vO[2])
		var intersection = addvec3(eO, scale3(t, subvec3(eO, vO)))

		return vec2(intersection[0], intersection[1])
	}
}

function tikzLines(lines) {
	var prog = `\\begin{tikzpicture}\n`
	
	for (var line of lines) {
		var [[fromX, fromY], [toX, toY]] = line
		prog += `\t\\draw (${fromX},${fromY}) -- (${toX},${toY});\n`
	}

	prog += `\\end{tikzpicture}`

	return prog
}

function wiggle(vert) {
	var [x, y, z] = vert

	x *= 2.2
	y *= 2.2
	z *= 2.2
	x += 0.5 * Math.cos(0.6 * y * Math.PI)
	z += 0.5 * Math.cos(0.6 * y * Math.PI)
	y *= (1 + 0.1 * y * y)
	x += 0.001 * Math.pow(Math.E, 0.8 * y + 4)
	x *= 80
	y *= 80
	z *= 80

	return vec3(x, y, z)
}

var wiggleSphere = transform(sphere, wiggle)

var morse1 = transform(makeFunctionGraph((x,y) => x * x + y * y), v => scale3(200, v))
var morse2 = transform(makeFunctionGraph((x,y) => x * x - y * y), v => scale3(200, v))
var morse3 = transform(makeFunctionGraph((x,y) => -x * x - y * y), v => scale3(200, v))
var graph1 = transform(makeFunctionGraph((x,y) => x * y), v => scale3(200, v))
var graph2 = transform(makeFunctionGraph((x,y) => -Math.sin(Math.pow(1.5 * x, 2) + Math.pow(1.5 * y, 2))), v => scale3(200, v))
var graph3 = transform(makeFunctionGraph((x,y) => Math.sqrt(1 - Math.pow(0.7 * x, 2) - Math.pow(0.7 * y, 2))), v => scale3(200, v))

cube = transform(cube, v => scale3(100, v))
cylinder = transform(cylinder, v => scale3(100, v))
torus = transform(torus, v => scale3(50, v))
sphere = transform(sphere, v => scale3(200, v))

var sphere2 = transform(sphere, function(vert) {
	var [x, y, z] = vert

	y *= 2
	x *= 1 + 0.00003 * y * y * (0.5 + 0.5 * Math.cos(2.0 * Math.atan2(y,x)))
	z *= 1 + 0.00003 * y * y * (0.5 + 0.5 * Math.sin(2.0 * Math.atan2(y,x)))

	return vec3(x,y,z)
})

var heights = []
for(var i = -350; i <= 350; i += 70)
//for(var i = -500; i <= 500; i += 80)
	heights.push(i+1)

var cutTorus = addContours(transform(wiggleSphere, v => [v[0], v[1], v[2]]), heights) //addContours(transform(torus, v => [v[0], -v[1], v[2]]), heights)  //removePointsInHalfSpace(torus, vec3(0,201,0), vec3(0,1,0))

// dist 30,000 is OK
var eye = vec3(1,1,1)
eye = scale3(30000 / norm(eye), eye)

//tikzLines(outline(wiggleSphere, makeCamera(eye, vec3(0,0,0), 400)))

console.log(tikzLines(outline(cutTorus, makeCamera(scale3(30000 / norm(vec3(0,400,-1000)), vec3(0,400,-1000)), vec3(0,0,0), 400))))