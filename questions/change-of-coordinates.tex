\documentclass{article}

\usepackage{import}
\import{/Users/Johannes/Documents/TeX/}{preamble.tex}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{systeme}
\usepackage{csquotes}
\newtheorem{example}{Eksempel}
\newtheorem{lemma}{Lemma}
\theoremstyle{definition}
\newtheorem{question}{Question}
\newcommand{\smalliff}{\Leftrightarrow}
\newcommand{\smallimplies}{\Rightarrow}

\title{Change of coordinates}
\author{Johannes Jensen}
\date{\today}

\begin{document}

\maketitle

Let $f \colon \R^n \to \R$ be a smooth map. Let $\mathbf{x} = (x_1, \ldots, x_n)$ be a coordinate system for $f$. Let $\mathbf{y} = (y_1, \ldots, y_n)$ be a new coordinate system for $f$, such that
\begin{equation}
	x_i = \phi_i(y_1, \ldots, y_n),
\end{equation}
where $\Phi = (\phi_1, \ldots, \phi_n)$ is a diffeomorphism of $\R^n$.  We consider $x_i$ as a function of $y_1, \ldots, y_n$. Thus we have that
\begin{equation}
	\frac{\partial f}{\partial y_i} (\mathbf{y}) = \sum_{k=1}^{n} \frac{\partial f}{\partial x_k}(\mathbf{x}) \frac{\partial x_k}{\partial y_i}(\mathbf{y}).
\end{equation}

We can define the Jacobian $J$ of $\Phi$ (which we shall call \textit{the coordinate change from $\mathbf{y}$ to $\mathbf{x}$} (in that order!)), a $n \times n$ matrix, with entries given by
\begin{equation} \label{jacobi}
	[J]_{ij} = \frac{\partial x_i}{\partial y_j},
\end{equation}
where $[\,\cdot\,]_{ij}$ denotes the $(i,j)$'th entry of a matrix. Let $H_{\mathbf{x}} f$ and $H_{\mathbf{y}} f$ denote the Hessian matrices, that is $n \times n$ matrices, given by
\begin{equation}
	[H_{\mathbf{x}} f]_{ij} = \frac{\partial^2 f}{\partial x_i \partial x_j}
	\quad
	\text{and}
	\quad
	[H_{\mathbf{y}} f]_{ij} = \frac{\partial^2 f}{\partial y_i \partial y_j}.
\end{equation}
We will say that a point $\mathbf{p} \in \R^n$ is a critical point of $f$, if
\begin{equation}
	\frac{\partial f}{\partial x_1}(\mathbf{p}) = \cdots = \frac{\partial f}{\partial x_n}(\mathbf{p}) = 0.
\end{equation}
We will say that a critical point $\mathbf{p}$ of $f$ is \textit{non-degenerate} if $\det(H_{\mathbf{x}}f)(\mathbf{p}) \ne 0$.

Now, we are ready to prove our result on the next page.

\newpage
\begin{lemma}
Let $\mathbf{p} \in \R^n$ be a critical point of $f$. Then
\begin{equation} \label{toprove}
	H_{\mathbf{y}} f(\mathbf{p}) = (J^T \, H_{\mathbf{x}}f \, J)(\mathbf{p}).
\end{equation}
\end{lemma}
\begin{proof}[Proof]
We have that
\begin{align}
	[H_{\mathbf{y}} f]_{ij} &= \frac{\partial^2 f}{\partial y_i \partial y_j} \\
	&= \frac{\partial}{\partial y_i} \para{\sum_{k=1}^n \frac{\partial f}{\partial x_k} \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \frac{\partial}{\partial y_i} \para{\frac{\partial f}{\partial x_k} \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \para{\frac{\partial f}{\partial x_k} \cdot \frac{\partial}{\partial y_i}\para{\frac{\partial x_k}{\partial y_j}} + \frac{\partial}{\partial y_i} \para{\frac{\partial f}{\partial x_k}} \cdot \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \para{\frac{\partial f}{\partial x_k} \frac{\partial^2 x_k}{\partial y_i \partial y_j}
	   + \frac{\partial x_k}{\partial y_j} \sum_{h=1}^n \para{\frac{\partial^2 f}{\partial x_h \partial x_k} \frac{\partial x_h}{\partial y_i}}}.
\end{align}
By assumption $\frac{\partial f}{\partial x_k}(\mathbf{p}) = 0$ for all $k$, so by evaluating the above at $\mathbf{p}$ we obtain
\begin{equation} \label{doublesum}
	[H_{\mathbf{y}} f]_{ij}(\mathbf{p})
	= \sum_{k=1}^n \sum_{h=1}^n \frac{\partial x_k}{\partial y_j}(\mathbf{p}) \frac{\partial^2 f}{\partial x_h \partial x_k}(\mathbf{p}) \frac{\partial x_h}{\partial y_i}(\mathbf{p}) = [J^T \, H_{\mathbf{x}} f \, J]_{ij}(\mathbf{p}),
\end{equation}
for all $i, j = 1, \ldots, n$, hence we get (\ref{toprove}).
\end{proof}

On the next page I have some questions.

\newpage
\begin{question}
Is the placement of $\mathbf{x}$ and $\mathbf{y}$ in equation (2) correct?
\end{question}

\begin{question}
Look at the double sum in (\ref{doublesum}). This sum appears on page 42 in my book, as equation (2.20). The author evaluates all the functions at $\mathbf{p}$. So, my question is: what does it mean to evaluate at $\mathbf{p}$ when a function is written in terms of $\mathbf{y}$? If we consider $f(\mathbf{x})$, then I understand $f(\mathbf{p})$ simply as the case where if $\mathbf{p} = (p_1, \ldots, p_n)$ then $\mathbf{x} = (p_1, \ldots, p_n)$ as numbers. But if we instead look at
\[
	f(x_1(y_1, \ldots, y_n), \ldots, x_n(y_1, \ldots, y_n)),
\]
then what does it mean to evaluate the above at $\mathbf{p}$? Does it mean that we find numbers $y^{*}_1, \ldots, y^{*}_n$, such that
\[
	x_i(y^{*}_1, \ldots, y^{*}_n) = p_i
\]
for all $i = 1, \ldots, n$? I want to make sense of the expressions $\displaystyle \frac{\partial f}{\partial y_i}(\mathbf{p})$ and $\displaystyle \frac{\partial x_i}{\partial y_j}(\mathbf{p})$.

An attempt I made: Evaluating $f(\mathbf{p})$, with $\mathbf{x} = \mathbf{p}$, is the same as having that $\mathbf{x}(\mathbf{y}) = \mathbf{p}$, and if we consider $\mathbf{x} = \Phi$, then $\Phi(\mathbf{y}) = \mathbf{p}$, and since $\Phi$ is invertible, we have that $\mathbf{y} = \Phi^{-1}(\mathbf{p})$. So if we want to evaluate a function $g(\mathbf{y})$ ``at $\mathbf{p}$", this must be the same as $g(\Phi^{-1}(\mathbf{p}))$? This is not nice to look at, so interpreting it as $g(\mathbf{p})$ is nicer on the eyes.
\end{question}

\begin{question}
This is related to the previous question. Consider the diffeomorphism $\Phi$ above. Then $f(\mathbf{x}) = f \circ \Phi(\mathbf{y})$. In an example from the book, where
\[
	f(x_1, x_2) = x_1 x_2
\]
and
\[
	f(x_1(y_1, y_2), x_2(y_1, y_2)) = y_1^2 - y_2^2,
\]
they look at the critical point $\mathbf{p} = (0, 0)$, the origin. What's lucky here is that after the coordinate change, the critical point we are looking at is \textit{still} $\mathbf{p} = (0,0)$. That is, we have that $\Phi$ fixes the critical point, $\Phi(\mathbf{p}) = \mathbf{p}$. I was wondering if this is an assumption you could make
\[
	\textit{Assume that $\Phi$ fixes the critical point $\mathbf{p}$.}
\]
The reason I had this thought was that it would solve Question 2. However, some research led me to Brouwer's fixed-point theorem, which has some pretty rigid conditions. I realize not every diffeomorphism from $\R^n \to \R^n$ has a fixed point, for example just translation $\mathbf{v} \mapsto \mathbf{v} + \mathbf{t}$ is obviously a diffeomorphism, but it has no fixed points.

This question is mostly out of interest, since all I need Lemma 1 for is to be able to say that non-degenerate critical points are stable under change of coordinates. But it would be nice to know if $\mathbf{p}$ is ``the same point", after change of coordinates.
%If you could show that it would always be possible to find a fix point $\Phi(\mathbf{q}) = \mathbf{q}$ for some $\mathbf{q}$, then assume that $\mathbf{p}$ is the origin (through a translation, if necessary), then defining $\Phi_1 = \Phi - \Phi(\mathbf{p})$, we get that $\Phi_1(\mathbf{p}) = \mathbf{p}$. Then Question 2 would be answered by the fact that $f(\Phi(\mathbf{p})) = f(\mathbf{p})$, so I wouldn't have to worry about when to write $\mathbf{p}$ or $\Phi^{-1}(\mathbf{p})$.

%Maybe this is not neccessary at all and not the case in general? Researching online I found Brouwer's fixed-point theorem, but it seems to have some pretty rigid assumptions. I realize not every diffeomorphism from $\R^n \to \R^n$ has a fixed point, for example just translation $\mathbf{v} \mapsto \mathbf{v} + \mathbf{t}$ is obviously a diffeomorphism, but it has no fixed points. Maybe it's a different story when working on manifolds?
\end{question}

\newpage
\subsection*{Andrew's reply}
Kære Johannes, \\ \\
Mange tak for filen. \\ \\
Angående dine spørgsmål \\ \\
Qn. 1 og 2:  Jeg tror at det er bedst skille punkter og koordinatsystemer ad. Så tænk på fed x og y som afbildninger
\[
	\mathbf{x}, \mathbf{y} \colon U \to V,
\]
hvor $U$ og $V$ er åbne mængder i $\R^n$ og $f$ er defineret på $V$ (det kan godt være $U = \R^n = V$, men det er rart at skille definitionsmængden og billedmængden ad).

Givet $p \in V$ (Andrew skrev her $U$, men det må være en tastefejl, jf. signaturen af afbildningerne nedenunder her), så er
\[
	q = \mathbf{x}^{-1}(p),
\]
og
\[
	r = \mathbf{y}^{-1}(p)
\]
dens repræsentation i de 2 koordinatsystemer, og
\[
	\Phi = \mathbf{x}^{-1} \circ \mathbf{y}
\]
så $\Phi(r) = q$.

``$f$ som funktion af $x$" bliver nu til ``$f \circ \mathbf{x}$", så $f(p) = (f \circ \mathbf{x})(q)$ og udtrykket
\[
	\frac{\partial f}{\partial x_i}
\]
er forkortet notation for
\[
	\frac{\partial (f \circ \mathbf{x})}{\partial x_i}
\]
med
\[
	\para{\frac{\partial f}{\partial x_i}}(p)= \para{\frac{\partial (f \circ \mathbf{x})}{\partial x_i}}(q)
\]
Tilsvarende er
\[
	\partial x_k / \partial y_i
\]
en forkortelse for
\[
	\frac{\partial (\phi)_k}{\partial y_i}
\]
Så (2) bliver mere i retning af
\[
	\frac{\partial (f \circ \mathbf{y})}{\partial y_i}
	= \sum_{k=1}^n \frac{\partial (f \circ \mathbf{x})}{\partial x_k}  \frac{\partial (\phi)_k}{\partial y_i}.
\]
og med fodpunkter er det
\[
	\frac{\partial (f \circ \mathbf{y})}{\partial y_i}(r)
	= \sum_{k=1}^n \frac{\partial (f \circ \mathbf{x})}{\partial x_k}(q)  \frac{\partial (\phi)_k}{\partial y_i}(r).
\]
Tilsvarende
\[
	[H_x f]_{ij} =\frac{\partial^2 (f \circ \mathbf{x})}{\partial x_i \partial x_j}
\]
Håber det hjælper. \\ \\
Mvh \\
Andrew

\end{document}