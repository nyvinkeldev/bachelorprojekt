\documentclass{article}

\usepackage{import}
\import{/Users/Johannes/Documents/TeX/}{preamble.tex}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{systeme}
\usepackage{csquotes}
\newtheorem{example}{Eksempel}
\newtheorem{lemma}{Lemma}
\theoremstyle{definition}
\newtheorem{question}{Question}
\newcommand{\smalliff}{\Leftrightarrow}
\newcommand{\smallimplies}{\Rightarrow}

\title{Uniqueness of index}
\author{Johannes Jensen}
\date{\today}

\begin{document}

\maketitle

I'm having difficulties making the following proof rigourous. The proof is based on the quadratic form proof from "A survey of modern algebra" by Birkhoff and Mac Lane.

First, I outline the proof, and then I will ask some questions.

\section*{Outline of proof}
We showed that there exists a coordinate system $\mathbf{y} = (y_1, \ldots, y_n)$ such that
\begin{equation}
	f = -y_1^2 - \cdots - y_{\lambda}^2 + y_{\lambda+1}^2 + \cdots y_n^2 \quad \text{ near } \mathbf{0}.
\end{equation}
Then to show uniqueness of $\lambda$, we assume (towards a contradiction) there is another coordinate system $\mathbf{z} = (z_1, \ldots, z_n)$ such that
\begin{equation}
	f = -z_1^2 - \cdots - z_{\mu}^2 + z_{\mu+1}^2 + \cdots z_n^2  \quad \text{ near } \mathbf{0},
\end{equation}
where $\mu \ne \lambda$. Computing the Hessians of these, we get
\begin{equation*}
	[H_{\mathbf{y}} f(\mathbf{0})]_{ij} = \begin{cases}
		0 & \text{ if } i \ne j \\
		-2 & \text{ if } i = j \leq \lambda \\
		2 & \text{ if } i = j > \lambda
	\end{cases}
	\quad \text{ and } \quad
	[H_{\mathbf{z}} f(\mathbf{0})]_{ij} = \begin{cases}
		0 & \text{ if } i \ne j \\
		-2 & \text{ if } i = j \leq \mu \\
		2 & \text{ if } i = j > \mu
	\end{cases}
\end{equation*}
Then consider two bases $(v_1, \ldots, v_n)$ and $(w_1, \ldots, w_n)$ of $\R^n$ and for an arbitrary $u \in \R^n$ write
\begin{equation}
	u = \sum_{i=1}^n \alpha_i v_i = \sum_{i=1}^n \beta_i w_i.
\end{equation}
Define the map $E \colon \R^n \to \R$ by how it acts on these bases:
\begin{align}
	E(u) &= -\alpha_1^2 - \cdots - \alpha_{\lambda}^2 + \alpha_{\lambda+1}^2 + \cdots \alpha_n^2 \\
	     &= -\beta_1^2 - \cdots - \beta_{\mu}^2 + \beta_{\mu+1}^2 + \cdots \beta_n^2.
\end{align}
The idea then is to consider two subspaces $S^{+}$ and $S^{-}$ given by
\begin{align*}
	S^{+} &= \left\{\sum_{i=1}^n \alpha_i v_i \,\, \Bigg\lvert \,\, \alpha_1 = \cdots = \alpha_{\lambda} = 0\right\} \subset \R^n \\
	S^{-} &= \left\{\sum_{i=1}^n \beta_i w_i \,\, \Bigg\lvert \,\,  \beta_{\mu+1} = \cdots = \beta_{n} = 0\right\} \subset \R^n.
\end{align*}
By construction, it is easy to see that
\begin{equation}
	u \in S^{+} \implies E(u) \geq 0
	\quad
	\text{ and }
	\quad
	u \in S^{-} \setminus \curly{0} \implies E(u) < 0.
\end{equation}
Since $\mu \ne \lambda$, we may assume $\mu > \lambda$. Then
\begin{equation}
	\dim(S^{+}) + \dim(S^{-}) = (n - \lambda) + \mu > (n - \lambda) + \lambda = n = \dim(\R^n).
\end{equation}
This gives us that $S^{+} \cap S^{-} \ne \curly{0}$\footnote{I haven't learned this in any course, but there's a proof in the ``A survey of modern algebra''. Seems intuitively clear though.}, so we may choose $u \in (S^{+} \cap S^{-}) \setminus \curly{0}$. Then $E(u) \geq 0$ and $E(u) < 0$, a contradiction. A similar argument is given if $\mu < \lambda$ (choose other $S^{+}$, $S^{-}$). Hence $\mu = \lambda$.

\section*{Questions}
I want to know what bases $(v_i)_i, (w_i)_i$ to choose for $E$ to make sense. Here's some initial thoughts I had about this:

In Milnor, he writes that the matrix $H_{\mathbf{y}} f(\mathbf{0})$ with the -2's and 2's on the diagonal represents $H_{\mathbf{y}} f$ with respect to the basis
\begin{equation} \label{eqqq}
	\para{\frac{\partial}{\partial y_1}}_p, \ldots, \para{\frac{\partial}{\partial y_n}}_p.
\end{equation}
Since we're embedded in $\R^N$, is the above then the same as (2.67) in Matsumoto? If it is, then I don't understand how it ``represents $H_{\mathbf{y}} f$'', since $H_{\mathbf{y}} f$ consists of double partials and $\para{\frac{\partial}{\partial y_i}}_p$ is only a single partial?

Continuing the other train of thought: If (\ref{eqqq}) is the same as (2.67) then it's the basis for the tangent space $T_p(M)$ with regards to the coordinate system $y_1, \ldots, y_n$. Formula (2.73) in Matsumoto then gives
\begin{equation}
	\para{\frac{\partial}{\partial z_i}}_p
	= \sum_{j=1}^n \frac{\partial y_j}{\partial z_i}(p) \para{\frac{\partial}{\partial y_j}}_p.
\end{equation}
So we can relate them through the coordinate chagne $\Phi = \mathbf{y}^{-1} \circ \mathbf{z}$. Then for a vector $v$ written in terms of (\ref{eqqq}) then $J_{\Phi}(p) v$ must be written in terms of $\para{\frac{\partial}{\partial z_1}}_p, \ldots, \para{\frac{\partial}{\partial z_n}}_p$.

Another thing I noted is that $E$ can be written as $u^T H_{\mathbf{y}} u$, and then we can write
\begin{equation}
	E(u) = u^T H_{\mathbf{y}} u = u^T (J_{\Phi}^T H_{\mathbf{z}} f J_{\Phi}) u = (J_{\Phi} u)^T H_{\mathbf{z}} f (J_{\Phi} u) = E(v),
\end{equation}
where $v = J_{\Phi} u$.

Maybe $E$ should have the signature $E \colon T_{p}(M) \to \R$ instead.

\end{document}