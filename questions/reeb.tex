\documentclass{article}

\usepackage{import}
\import{/Users/Johannes/Documents/TeX/}{preamble.tex}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{systeme}
\usepackage{csquotes}
\newtheorem{example}{Eksempel}
\newtheorem{lemma}{Lemma}
\newtheorem{thm}{Theorem}
\theoremstyle{definition}
\newtheorem{question}{Question}
\newcommand{\smalliff}{\Leftrightarrow}
\newcommand{\smallimplies}{\Rightarrow}

\title{Reeb's sphere theorem}
\author{Johannes Jensen}
\date{\today}

\begin{document}

\maketitle

We want to prove the following:
\begin{thm}
Let $M$ be a compact $n$-manifold without boundary embedded in $\R^N$ and let $f \colon M \to \R$ be a Morse function on $M$ with exactly two critical points. Then $M$ is homeomorphic to a sphere $S^n$.
\end{thm}
\begin{proof}[Proof sketch]
First, we note that by a $n$-sphere we mean
\[
	S^n = \makeset{(x_1, \ldots, x_n, x_{n+1}) \in \R^{n+1}}{x_1^2 + \cdots + x_n^2 + x_{n+1}^2 = 1}.
\]
Let $p_A, p_B$ be the critical points of $f$. We may assume that $p_A$ is the minimum, $p_B$ is the maximum and that $f(p_A) = 0$ and $f(p_B) = 1$.

Using Morse's Lemma, we may find a coordinate neighbourhood $U$ containing $p_A$ with coordinates $y_1, \ldots, y_n$ such that
\[
	f(y_1, \ldots, y_n) = y_1^2 + \cdots + y_n^2
\]
on $U$ (This is part more detailed in my actual report). Let $\delta > 0$ such that $B = \overline{B}_\delta(p_A) \subset U$. Then let
\[
	\varepsilon = \max\makeset{f(p)}{p \in B},
\]
which exists since $B$ is compact. Then for all points in $B$ we have that
\[
	y_1^2 + \cdots + y_n^2 \leq \varepsilon.
\]
It follows that $B$ is homeomorphic to a closed $n$-disk, that is the set
\[
	D = \makeset{(x_1, \ldots, x_n) \in \R^n}{x_1^2 + \cdots + x_n^2 \leq 1}.
\]
Note also that on $f^{-1}(\varepsilon)$ we have that
\[
	y_1^2 + \cdots + y_n^2 = \varepsilon.	
\]
So
\[
	f^{-1}(\varepsilon) \cong S^{n-1}.
\]
Note also that
\[
	\partial B = f^{-1}(\varepsilon).
\]

Let $X$ be the global gradient-like vector field which we have proved exists on $M$. Then define
\[
	Y = \frac{X}{X(f)},
\]
which is well-defined on $M \setminus \curly{p_A, p_B}$ (since $X_p(f) = 0$ if and only if $p$ is a critical point), that is on the open set $f^{-1}(0, 1)$. The integral curves of $Y$ move away from the point $p_A$ and move towards to the point $p_B$, and for every point $p \in M \setminus \curly{p_A, p_B}$ we have that there exists an integral curve $\gamma_p$ of $Y$ which satisfies that
\begin{align*}
	f(\gamma_p(0)) &= f(p) \\
	f(\gamma_p(t)) &= f(p) + t,
\end{align*}
for all sufficiently small $t > 0$. (Since $0 < f(p) < 1$, there is always a number strictly between $f(p)$ and 1)

Using the integral curves of $Y$ we may prove that every level set $f^{-1}(\ell)$ where $\ell \in (0,1)$ is homeomorphic to a copy of $S^{n-1}$.

Now we construct a homeomorphism $\Phi \colon M \to S^n$. For a point $p \in B$, we define
\[
	\Phi(p) = \para{y_1 / \sqrt{\varepsilon}, \ldots, y_n / \sqrt{\varepsilon}, - \sqrt{1 - \frac{f(p)}{\varepsilon}}}.
\]
Each such point satisfies that
\begin{align*}
	\frac{y_1^2 + \cdots + y_n^2}{\varepsilon} + 1 - \frac{f(p)}{\varepsilon} = 1,
\end{align*}
since $f(p) = y_1^2 + \cdots + y_n^2$. So $\Phi(p) \in S^n$ for all $p \in B$. Now, note that if $f(p) = 0$, then $p = p_A$, so 
\[
	\Phi(p) = (0, \ldots, 0, -1).
\]
If $f(p) = \varepsilon$, that is if $p \in \partial B$, then
\[
	\Phi(p) = (y_1/\sqrt{\varepsilon}, \ldots, y_n/\sqrt{\varepsilon}, 0).
\]
It follows that $\Phi$ maps every point of $B$ to the ``southern'' hemisphere
\[
	\makeset{(x_1, \ldots, x_n, x_{n+1}) \in S^n}{x_{n+1} \leq 0}
\] 
of $S^n$, so that it is covered by points from $B$.

Now let $p \in M_{[\varepsilon,1]}$, then we want to cover the ``northern'' hemisphere of $S^n$. Define $M_{[\varepsilon,1]} \to \R$ maps
\[
	\beta(p) = \sqrt{\frac{f(p) - \varepsilon}{1 - \varepsilon}}
\]
and
\[
	\alpha(p) = \sqrt{\frac{1 - \beta(p)^2}{\varepsilon}},
\]
and note that they are continuous as a composition of continuous maps, and well-defined since $0 < \varepsilon < 1$. Then set
\[
	\Phi(p) = (\alpha y_1, \ldots, \alpha y_n, \beta),
\]
where $y_1, \ldots, y_n$ are the coordinates of $\partial B$ that you reach if you follow the integral curve of $Y$ that connects $\partial B$ and $p$ backwards, here assuming that $f(p) < 1$. This operation is continuous. If $p \in \partial B$ this is just $p$ written in the coordinates $y_1, \ldots, y_n$. If $f(p) = 1$, that is if $p = p_B$, then there isn't a unique curve to follow back to $\partial B$, but we instead just define that
\begin{equation} \label{tojustify}
	\Phi(p_B) = (0, \ldots, 0, 1),
\end{equation}
which we justify later. Then note that
\begin{align*}
	(\alpha y_1)^2 + \cdots + (\alpha y_n)^2 + \beta^2 &= \alpha^2 (y_1^2 + \cdots + y_n^2) + \beta^2 \\
	&= \alpha^2 \varepsilon + \beta^2 \\
	&= \para{\frac{1 - \beta^2}{\varepsilon}}\varepsilon + \beta^2 \\
	&= 1 - \beta^2 + \beta^2 \\
	&= 1,
\end{align*}
so $\Phi(p) \in S^n$. Note that if $f(p) = \varepsilon$ then $\beta(p) = 0$ and if $f(p) = 1$ then $\beta(p) = 1$, so $\Phi$ maps every point of $M_{[\varepsilon,1]}$ to the ``northern'' hemisphere
\[
	\makeset{(x_1, \ldots, x_n, x_{n+1}) \in S^n}{x_{n+1} \geq 0}.
\]
Finally, note that if $f(p) = \varepsilon$ then $\alpha = 1/\sqrt{\varepsilon}$ and $\beta = 0$, so
\[
	\Phi(p) = (\alpha y_1, \ldots, \alpha y_n, \beta) = (y_1 / \sqrt{\varepsilon}, \ldots, y_n / \sqrt{\varepsilon}, 0),
\]
which agrees with how we defined $\Phi$ on $\partial B = B \cap M_{[\varepsilon, 1]}$.

Now we justify (\ref{tojustify}). Let $\curly{p_i}_{i=1}^{\infty}$ be a sequence of points in $M_{[\varepsilon,1)}$ that converges to $p_B$, which also satisfies that
\[
	\Phi(p_i) = (\alpha(p_i) y_1, \ldots, \alpha (p_i) y_n, \beta(p_i))
\]
for all $i$. That is, they all lie on the same integral curve. Then note that
\begin{align*}
	\lim_{i \to \infty} \alpha(p_i) &= \alpha(p_B) = 0 \\
	\lim_{i \to \infty} \beta(p_i) &= \beta(p_B) = 1
\end{align*}
so
\[
	(\alpha(p_i) y_1, \ldots, \alpha (p_i) y_n, \beta(p_i)) \to (0, \ldots, 0, 1)
\]
for $i \to \infty$. This means that no matter which direction we approach $p_B$ from, we get the same result, so the choice of $\Phi(p_B)$ in (\ref{tojustify}) is well-defined. So $\Phi$ is continuous.

Now, since $\Phi$ is injective and surjective, and since $M$ is compact, we get by Prop. (???) in [?] that since $M$ is compact and $S^n$ is Hausdroff, then $\Phi^{-1}$ is continuous. Thus $\Phi$ is a homeomorphism.


\end{proof}

\end{document}