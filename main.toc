\contentsline {chapter}{Abstract}{i}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Literature}{1}
\contentsline {section}{\numberline {1.2}Notation abuse}{1}
\contentsline {section}{\numberline {1.3}Vector fields}{2}
\contentsline {chapter}{\numberline {2}The Morse Lemma}{5}
\contentsline {section}{\numberline {2.1}Critical points}{5}
\contentsline {section}{\numberline {2.2}A result from calculus}{7}
\contentsline {section}{\numberline {2.3}A result from linear algebra}{8}
\contentsline {section}{\numberline {2.4}The Morse Lemma}{8}
\contentsline {section}{\numberline {2.5}Corollaries}{12}
\contentsline {chapter}{\numberline {3}Gradient-like vector fields}{14}
\contentsline {section}{\numberline {3.1}Definition}{14}
\contentsline {section}{\numberline {3.2}Existence of a global gradient-like vector field}{14}
\contentsline {chapter}{\numberline {4}A fundamental theorem of Morse Theory}{19}
\contentsline {section}{\numberline {4.1}Integral curves}{19}
\contentsline {section}{\numberline {4.2}A fundamental theorem of Morse Theory}{20}
\contentsline {chapter}{\numberline {5}Reeb's sphere theorem}{25}
\contentsline {section}{\numberline {5.1}Reeb's sphere theorem}{25}
