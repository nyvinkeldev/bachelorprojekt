\documentclass{article}

\input{preamble.tex}

\title{Bachelor Thesis}
\author{Johannes Jensen}
\date{\today}

\begin{document}

\maketitle

Unless otherwise stated, most of the content will (as of right now) be based on \citep{matsumoto}.

\section{The Morse lemma (2D)}
We want to state and prove the Morse lemma, but first we need some definitions:

\begin{defn}[Critical point]
Let $f \in C^{\infty}(\R^2, \R)$. We say that a point $p_0 \in \R^2$ is a \textit{critical point} of $f$ if
\[
	\pderive{f}{x}(p_0) = 0
	\quad
	\text{and}
	\quad
	\pderive{f}{y}(p_0) = 0.
\]
\end{defn}

\begin{defn}[Types of critical points]
Let $f \in C^{\infty}(\R^2, \R)$. Suppose that $p_0 \in \R^2$ is a critical point of $f$. Let
\[
	H_f = \begin{pmatrix}
		\ppderive{f}{x}(p_0)    & \pdderive{f}{x}{y}(p_0) \\
		\pdderive{f}{y}{x}(p_0) & \ppderive{f}{y}(p_0)
	\end{pmatrix}
\]
denote the Hessian of $f$ at $p_0$. We say that $p_0$ is a \textit{non-degenerate} critical point of $f$ if $\det(H_f) \ne 0$. If $\det(H_f) = 0$, we say that $p_0$ is a \textit{degenerate} critical point of $f$.
\end{defn}

\begin{rmk}
It is a well known fact of multivariate calculus that ${\pdderive{f}{x}{y} = \pdderive{f}{y}{x}}$, cf. \citep{duPlessisSwann} Lemma 4.29, hence $H_f$ is a symmetric matrix, and thus behaves very nicely as we shall see later.
\end{rmk}

We will need the following lemma:

\begin{lem} \label{lem:DiffeomorphicHessianRelation}
Let $f, g \in C^{\infty}(\R^2, \R)$ and let $\Phi \colon \R^2 \to \R^2$ be a diffeomorphism. Let $p_0$ be a critical point of $f$ and assume that $f(\Phi(p)) = g(p)$ for all $p \in \R^2$. \todo{Maybe the assumption here on $\Phi$ should probably instead be that it is a diffeomorphism on some small neighbourhood $U \subseteq \R^2$ containing the critical point $p_0$, so $f \circ \Phi = g$ only on $U$.} Then
\[
	H_g(p_0) = J_{\Phi}(p_0)^T H_f(p_0) J_{\Phi}(p_0),
\]
where $J_{\Phi}$ denotes the Jacobian of $\Phi$.
\end{lem}
\begin{proof}
\todo{Start on proof.}
\end{proof}

\begin{rmk}[Change of coordinates] \label{rmk:ChangeOfCoordiantes}
Let $f(x,y), g(x,y)$ and \[ \Phi = (\Phi_1(x,y), \Phi_2(x,y)) \] satisfy the conditions in the above lemma. By denoting $X = \Phi_1$ and $Y = \Phi_2$, we may write $g(x,y)$ as $f(X,Y)$, so that we may consider $\Phi$ as the coordinate change from the local coordinates $(x,y)$ of $f$ around a point $p \in \R^2$ to the local coordinates $(X,Y)$ of $f$ around the point $p$.
\end{rmk}

\begin{cor}
Critical points (and their degeneracy) are stable under the change of coordinates.
\end{cor}
\begin{proof}
Let $f, g, \Phi$ satisfy the conditions in Lemma \ref{lem:DiffeomorphicHessianRelation}. Let $p_0 \in \R^2$ be a critical point of $f$. Then since $\Phi$ is a diffeomorphism, we have that $J_{\Phi}$ is invertible around $p_0$, so $\det(J_{\Phi}(p_0)) \ne 0$. Then
\begin{align*}
	\det(H_g(p_0)) &= \det(J_{\Phi}(p_0)^T) \det(H_f(p_0) \det(J_{\Phi}(p_0)) \\
	               &= \det(J_{\Phi}(p_0))^2 \det(H_f(p_0)),
\end{align*}
by the multiplicative property of determinants, and since $\det(A^T) = \det(A)$, for all $A \in \text{Mat}_2(\R)$. Hence the determinant of $H_f(p_0)$ is zero if and only if the determinant of $H_g(p_0)$ is zero. 
\end{proof}

From now on instead of using $f, g$ and $\Phi$ like previously to describe coordinate changes, we will talk about the local coordinate systems $(x,y)$ and $(X,Y)$ of $f$ around a point instead as in Remark \ref{rmk:ChangeOfCoordiantes}.

\begin{lem} \label{lem:FundamentalFactFromCalculus}
Let $f \in C^{\infty}(U, \R)$, for some open neighbourhood $U \subseteq \R^2$ of $(0,0)$, with $f(0, 0) = 0$. Then there exists functions $g, h \in C^{\infty}(V, \R)$, for some open neighbourhood $V \subseteq U$ of $(0,0)$ such that
\[
	f(x,y) = x \, g(x, y) + y \, h(x, y)
\]
on $V$, and
\[
	\pderive{f}{x}(0,0) = g(0,0), \quad \pderive{f}{y}(0,0) = h(0,0).
\]
\end{lem}
\begin{proof}
Assume, without loss of generality, that $f$ is defined on $\R^2$. Fix a point $(x_0, y_0) \in \R^2$. Now consider the map
\begin{align*}
	m \colon \R &\to \R \\
	t &\mapsto f(t \, x_0, t \, y_0).
\end{align*}
By a corollary to the fundamental theorem of calculus (\citep{ETP}, Theorem 8.18) we have that
\[
	\int_{0}^{1} \left(\frac{d}{dt} m(t) \right) dt = m(1) - m(0) = f(x_0, y_0),
\]
where we used that $m(0) = f(0, 0) = 0$. The chain rule (\citep{ETP}, Theorem 9.14) gives us that
\[
	\frac{d}{dt} m(t) = x_0 \pderive{f}{x}(t x_0, t y_0) + y_0 \pderive{f}{x}(t x_0, t y_0)
\]
and then by using the fact that integration is linear, we see that we may choose
\[
	g(x, y) = \int_{0}^{1} \pderive{f}{x}(t x, t y) dt
	\quad
	\text{and}
	\quad
	h(x, y) = \int_{0}^{1} \pderive{f}{y}(t x, t y) dt.
\]
Thus $f(x_0,y_0) = x_0 g(x_0, y_0) + y_0 h(x_0, y_0)$ and by construction we have that $g(0,0) = \pderive{f}{x}(0,0)$ and $h(0,0) = \pderive{f}{y}(0,0)$. Since $(x_0, y_0)$ was arbitrary, we see that the above choice of $g$ and $h$ works on all of $\R^2$.
\end{proof}

We are now ready to state and prove:

\begin{thm}[The Morse lemma] \label{thm:MorseLemma2D}
Let $f \in C^{\infty}(\R^2, \R)$ and let $p_0 \in \R^2$ be a non-degenerate critical point of $f$. Then there exists local coordinates $(X, Y)$ of $f$ such one of the following is true
\begin{enumerate}
	\item $f(X, Y) =  X^2 + Y^2 + c$
	\item $f(X, Y) = -X^2 + Y^2 + c$
	\item $f(X, Y) = -X^2 - Y^2 + c$
\end{enumerate}
where $c = f(p_0)$ and $p_0 = (0,0)$.
\end{thm}
\begin{proof}
Choose a local coordinate system $(x,y)$ near $p_0$. We may assume that $p_0$ is the origin, since we could just apply a translation $p \mapsto p - p_0$ to bring $p_0$ to the origin. We may also assume that $f(p_0) = 0$, since if $f(p_0) \ne 0$, then we could just look at $f_1(x,y) = f(x,y) - f(p_0)$ instead, which has the property $f_1(p_0) = 0$. Now, we want to show that we may assume that
\begin{equation} \label{thm:MorseLemma2D:eq1}
	\ppderive{f}{x}(p_0) \ne 0.
\end{equation}
If (\ref{thm:MorseLemma2D:eq1}) is already true, then we may proceed. If instead $\ppderive{f}{y}(p_0) \ne 0$, then by interchanging the $x$-axis and $y$-axis we get (\ref{thm:MorseLemma2D:eq1}). Finally, suppose that $\ppderive{f}{x}(p_0) = \ppderive{f}{y}(p_0) = 0$. Then since $H_f(p_0)$ is a symmetric matrix, we must have that
\[
	H_{f(x,y)}(p_0) = \begin{pmatrix} 0 & a \\ a & 0 \end{pmatrix},
\]
for some $a \in \R$. Now, since $p_0$ is non-degenerate, we have that $\det(H_{f(x,y)}(p_0)) \ne 0$, so it must be the case that $a \ne 0$. Now, we introduce the coordinate system $(X,Y)$ by
\[
	x = X - Y, \quad y = X + Y,
\]
then
\[
	J_{\Phi} = \begin{pmatrix}
		1 & -1 \\
		1 & 1
	\end{pmatrix}
\]
is the Jacobian of the change from coordinates from $(X,Y)$ to $(x,y)$. Then Lemma \ref{lem:DiffeomorphicHessianRelation} gives us that at $p_0$ we have
\[
	H_{f(X,Y)} = J_{\Phi}^T H_{f(x,y)} J_{\Phi}
	= \begin{pmatrix}
		2a & 0   \\
		0  & -2a
	\end{pmatrix},
\]
hence $\ppderive{f}{X}(p_0) \ne 0$ and $\ppderive{f}{Y}(p_0) \ne 0$. So by making an appropriate coordinate change, we see that we may assume that (\ref{thm:MorseLemma2D:eq1}) is true.

By Lemma \ref{lem:FundamentalFactFromCalculus} we may write $f(x,y) = x \, g(x, y) + y \, h(x, y)$ in some neighbourhood around the origin such that $\pderive{f}{x}(0,0) = g(0,0)$ and $\pderive{f}{y}(0,0) = h(0,0)$. Since $p_0 = (0,0)$ is a critical point, we have that $g(0,0) = h(0,0) = 0$, so we may apply Lemma \ref{lem:FundamentalFactFromCalculus} to $g$ and $h$ to obtain functions $h_{11}, h_{12}, h_{21}$ and $h_{22}$ such that
\[
	g(x,y) = x \, h_{11}(x,y) + y \, h_{12}(x,y)
\]
and
\[
	h(x,y) = x \, h_{21}(x,y) + y \, h_{22}(x,y).
\]
Let $H_{11} = h_{11}$, $H_{12} = (h_{12} + h_{21})/2$ and $H_{22} = h_{22}$, then we obtain
\[
	f(x,y) = x^2 H_{11} + 2xy H_{12} + y^2 H_{22}.
\]
Using the above form for $f$, a calculation which only involves the product rule for partial derivatives gives us that
\begin{align*}
	\ppderive{f}{x}(0,0) &= 2 H_{11}(0,0), \\
	\pdderive{f}{x}{y}(0,0) &= \pdderive{f}{y}{x}(0,0) = 2 H_{12}(0,0), \\
	\ppderive{f}{y}(0,0) &= 2 H_{22}(0,0).
\end{align*}
By assumption, we have that $\ppderive{f}{x}(0,0) \ne 0$, hence $H_{11}(0,0) \ne 0$. Since $H_{11}$ is continuous, then there exists an open neighbourhood $U \subseteq \R^2$ around $(0,0)$ such that $H_{11}$ is non-zero on $U$.

We define a new $x$-coordinate $X$ on $U$ by
\[
	X = \sqrt{\abs{H_{11}}} \left(x + \frac{H_{12}}{H_{11}} y\right),
\]
but we keep the $y$-coordinate as it is. The Jacobian for the coordinate change from $(x,y)$ to $(X,y)$ evaluated at $(0,0)$ is then given by
\[
	J(0,0) = \begin{pmatrix}
		\pderive{X}{x} & \pderive{X}{y} \\
		\pderive{y}{x} & \pderive{y}{y}
	\end{pmatrix}(0,0)
	= \begin{pmatrix}
		\sqrt{\abs{H_{11}(0,0)}} & \pderive{X}{y}(0,0) \\
		0 & 1
	\end{pmatrix}.
\]
Hence $\det(J(0,0)) = \sqrt{\abs{H_{11}(0,0)}} \ne 0$, so we have that the coordinate system $(X,y)$ is well-defined.
\todo{Make this more rigorous, so we avoid writing things like $\pderive{y}{x}$ and $\pderive{y}{y}$.}
\todo{Finish proof.}
\end{proof}

\begin{cor}
Let $f \in C^{\infty}(\R^2, \R)$. Then every non-degenerate critical point of $f$ is isolated.
\end{cor}
\begin{proof}
Let $p_0 \in \R^2$ be a non-degenerate critical point of $f$. Then by Theorem \ref{thm:MorseLemma2D} there exists a neighbourhood $U \subseteq \R^2$ of $p_0$ and coordinates $(x, y)$ such that we on $U$ may write
\[
	f(x, y) = \alpha x^2 + \beta y^2 + f(p_0),
\]
with $\alpha, \beta \in \curly{\pm 1}$. Then
\[
	\pderive{f}{x} = 2 \alpha x
	\quad
	\text{and}
	\quad
	\pderive{f}{y} = 2 \beta y,
\]
which only vanishes when $x = y = 0$, hence only at $p_0$. Thus $p_0$ is isolated. 
\end{proof}

\bibliographystyle{plain}
\bibliography{references}

\end{document}