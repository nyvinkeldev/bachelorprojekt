\chapter{Reeb's sphere theorem}
\section{Reeb's sphere theorem}

%\section{Reeb's sphere theorem}
We will need the following theorem, which is Theorem 2.39 in \citep{duPlessisSwannTop}:
\begin{thm} \label{easyhomeomorphisms}
Let $X$ and $Y$ be topological spaces. Assume that $X$ is compact and that $Y$ is Hausdorff. If a continuous map $f \colon X \to Y$ is bijective then the inverse map $f^{-1} \colon Y \to X$ is also continuous.
\end{thm}

\begin{defn}[$n$-sphere]
A $n$-sphere is a set which is homeomorphic to
\[
	S^n = \makeset{(x_1, \ldots, x_n, x_{n+1}) \in \R^{n+1}}{x_1^2 + \cdots + x_n^2 + x_{n+1}^2 = 1}.
\]
\end{defn}

\begin{figure}[h!]
\centering
\scalebox{0.8}{\input{figures/reebmanifold.tex}}
\caption{A manifold which is homeomorphic to a sphere}
\end{figure}

\begin{thm}
Let $M$ be a compact $n$-manifold without boundary embedded in $\R^N$ and let $f \colon M \to \R$ be a Morse function on $M$ with exactly two critical points. Then $M$ is homeomorphic to a sphere $S^n$.
\end{thm}
\begin{proof}
Let $p_{A}$ and $p_{B}$ be the 2 critical points of $f$, with critical values $c_A = f(p_A)$ and $c_B = f(p_B)$. Assume that $c_A < c_B$. Note that $c_A$ and $c_B$ must neccessarily be the minimum and maximum of $f$, respectively. We may assume that $c_A = 0$ and $c_B = 1$, since we may just consider
\begin{equation}
	g(p) = \frac{f(p) - c_A}{c_B - c_A}
\end{equation}
instead, since if $x_1, \ldots, x_n$ are some local coordinates of $p$, then
\begin{equation}
	\frac{\partial g}{\partial x_i}(p) = \frac{1}{c_B - c_A} \cdot \frac{\partial f}{\partial x_i}(p)
\end{equation}
for all $i = 1, \ldots, n$, so $f$ and $g$ have the same critical points as $g$ and the determinants of their Hessians differ only by a constant non-zero factor. Thus we assume that $c_A = 0$ and $c_B = 1$.

Morse's Lemma gives us that there exists a coordinate neighbourhood $U$ of $p_A$ with local coordinates $y_1, \ldots, y_n$ such that $p_A$ is represented by $y_1 = \cdots = y_n = 0$ and there exists a unique index $\lambda$ such that
\begin{equation}
	f(y_1, \ldots, y_n) = -y_1^2 - \cdots - y_{\lambda}^2 + y_{\lambda+1}^2 + \cdots + y_n^2.
\end{equation}
If $\lambda > 0$, then for a $y_1 \ne 0$ we have that
\[
	f(y_1, 0, \ldots, 0) = -y_1^2 < 0 = c_A,
\]
which contradicts the fact that $c_A$ is the minimum of $f$. Hence $\lambda = 0$. Thus
\begin{equation}
	f(y_1, \ldots, y_n) = y_1^2 + \cdots + y_n^2
\end{equation}
on $U$. Since $U$ is open, we may choose $\delta > 0$ such that
\begin{equation}
	B := \overline{B_{\delta}(p_A)} \subseteq U.
\end{equation}
Now let $\varepsilon = \delta^2$, then
\begin{equation}
	f(y_1, \ldots, y_n) = y_1^2 + \cdots + y_n^2 \leq \varepsilon
\end{equation}
on $B$. It follows that $B$ is homemorphic to a closed $n$-disk
\[
	D = \makeset{(x_1, \ldots, x_n) \in \R^n}{x_1^2 + \cdots + x_n^2 \leq 1}.
\]
Note that on $\partial B$ we have that
\[
	y_1^2 + \cdots + y_n^2 = \varepsilon,
\]
so $f^{-1}(\varepsilon) = \partial B$ is homeomorphic to $S^{n-1}$.

Now let $X$ denote a global gradient-like vector field on $M$, which exists by Theorem \ref{existenceofglobalgradientlike}. Then define
\[
	Y = \frac{X}{X(f)},
\]
and we note that $X_p(f) = 0$ if and only if $p \in \curly{p_A, p_B}$, so $Y$ is well-defined everywhere on $M$ except at the critical points of $f$. Now, as we did earlier, we may construct a family of integral curves of $Y$ given by
\[
	\gamma_p(t),
\]
with $p \in M$ and $t \in \R$, satisfying that $\gamma_p(0) = p$ and
\[
	f(\gamma_p(t)) = f(p) + t,
\]
for $t$ small enough that $f(p) + t \in (0, 1)$. We note that since the integral curves do not cross, and the map $(p, t) \mapsto \gamma_p(t)$ is continuous, we have for each $\ell \in (0,1)$ a homeomorphism
\begin{align*}
	\theta_{\ell} \colon f^{-1}(\ell) &\to \partial B \\
	p &\mapsto \gamma_p(\varepsilon - \ell).
\end{align*}
It follows that every level set $f^{-1}(\ell)$, when $0 < \ell < 1$, is homemorphic to $S^{n-1}$. Now define the map
\[
	\theta(p) = \theta_{f(p)}(p)
\]
on $M \setminus \curly{p_A, p_B}$, which is continuous.

Now we are ready to construct a homeomorphism $\varphi \colon M \to S^n$. We define $\varphi$ on $B = M_{[0,\varepsilon]}$ and on $M_{[\varepsilon,1]}$ separately, and then we argue that they agree on their overlap.

Let $p \in B$. Then we define
\[
	\varphi(p) = \para{\frac{1}{\sqrt{\varepsilon}}(y_1, \ldots, y_n), -\sqrt{1 - \frac{f(p)}{\varepsilon}}}.
\]
The right hand side satisfies that
\[
	\frac{y_1^2 + \cdots + y_n^2}{\varepsilon} + 1 - \frac{f(p)}{\varepsilon} = 1,
\]
since $f(p) = y_1^2 + \cdots + y_n^2$. Thus $\varphi(p) \in S^n$. Now, note that if $f(p) = 0$, then $p = p_A \in B$, so
\[
	\varphi(p_A) = (0, \ldots, 0, -1).
\]
If $f(p) = \varepsilon$, then $p \in \partial B$, and then
\[
	\varphi(p) = \para{\frac{1}{\sqrt{\varepsilon}}(y_1, \ldots, y_n), 0}.
\]
It follows that $\varphi(B)$ covers the ``southern'' hemisphere of $S^n$, that is points where the last coordinate of $S^n$ are in the range $[-1,0]$.

Now let $p \in M_{[\varepsilon, 1]}$. Then we want to cover the ``northern'' hemisphere of $S^n$. We define two $M_{[\varepsilon,1]} \to \R$ maps
\[
	\beta(p) = \sqrt{\frac{f(p) - \varepsilon}{1 - \varepsilon}}
	\quad
	\text{and}
	\quad
	\alpha(p) = \sqrt{\frac{1 - \beta(p)^2}{\varepsilon}},
\]
and note that these are continuous as compositions of continuous maps, and well-defined since $0 < \varepsilon < 1$. Then set
\[
	\varphi(p) = (\alpha(p) \theta(p), \beta(p))
\]
if $\varepsilon \leq f(p) < 1$. That is, for every point in $q \in M_{[\varepsilon,1)}$ we follow the integral curve $\gamma$ of $Y$ which passes through $q$ back to the unique point it passes through in $\partial B$. If $f(p) = 1$, then we set
\begin{equation} \label{tojustifyyy}
	\varphi(p) = (0, \ldots, 0, 1) \in S^n,
\end{equation}
which we justify later. Writing $\theta(p) = (y_1, \ldots, y_n)$ (using the coordinates of $B$), we note that
\begin{align*}
	(\alpha y_1)^2 + \cdots + (\alpha y_n)^2 + \beta^2 &= \alpha^2 (y_1^2 + \cdots + y_n^2) + \beta^2 \\
	&= \alpha^2 \varepsilon + \beta^2 \\
	&= \para{\frac{1 - \beta^2}{\varepsilon}}\varepsilon + \beta^2 \\
	&= 1 - \beta^2 + \beta^2 \\
	&= 1.
\end{align*}
Thus $\varphi(p) \in S^n$. Note that if $f(p) = \varepsilon$, then $\beta(p) = 0$ and if $f(p) = 1$ then $\beta(p) = 1$, so $\varphi(M_{[\varepsilon,1]})$ covers the ``northern'' hemisphere of $S^n$, if $\varphi$ is continuous. We now show that $\varphi$ is continuous. If $f(p) = \varepsilon$, then $\alpha = 1/\sqrt{\varepsilon}$, and $\beta = 0$, so if we write $\theta(p) = (y_1, \ldots, y_n)$ then
\[
	\varphi(p) = (\alpha y_1, \ldots, \alpha y_n, \beta) = \para{\frac{1}{\sqrt{\varepsilon}}(y_1, \ldots, y_n), 0}.
\]
Thus the two definitions of $\varphi$ at $\partial B$ agree. Now we justify (\ref{tojustifyyy}). Let $\curly{p_i}_{i=1}^{\infty}$ be a sequence of points in $M$ which converges to $p_B$. Then since $f$ is continuous, we have that $f(p_i) \to f(p_B) = 1$ for $i \to \infty$. We may thus assume that $f(p_i) \geq \varepsilon$ for all sufficiently large $i$. Then for all such $i$ we have
\[
	\varphi(p_i) = (q_i, \beta(p_i)), 
\]
where $q_i \in \R^n$, satisfying
\[
	\norm{q_i}^2 + \beta(p_i)^2 = 1,
\]
so
\[
	\norm{q_i} = \sqrt{1 - \beta(p_i)^2} \to \sqrt{1 - \beta(p_B)^2} = 0
\]
for $i \to \infty$. So the definition
\[
	\varphi(p_B) = (0, \ldots, 0, 1)
\]
is justified, so $\varphi$ is continuous.

Now, we note that $\varphi$ is injective and surjective, and since $M$ is compact and $S^n$ is Hausdorff, then Theorem \ref{easyhomeomorphisms} gives us that $\varphi^{-1}$ is continuous, hence $\varphi$ is a homeomorphism.
\end{proof}