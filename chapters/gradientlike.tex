\chapter{Gradient-like vector fields}
\section{Definition}
\begin{defn}[Gradient-like vector field] \label{gradientlikedefn}
A vector field $X$ is a \textit{gradient-like vector field} for a Morse function $f \colon M \to \R$ if it satisfies the following:
\begin{enumerate}[label=(\roman*)]
	\item $X(f) > 0$ away from the critical points of $f$.
	\item If $p_0$ is a critical point of $f$ with the index $\lambda$, then there exists a neighbourhood $V$ of $p$ with a coordinate system $(x_1, \ldots, x_n)$ such that
	\[
		f = -x_1^2 - \cdots - x_{\lambda}^2 + x_{\lambda+1}^2 + \cdots + x_n + f(p_0),
	\]
	and $X$ can be written as its gradient vector field:
	\[
		X = -2x_1 \frac{\partial}{\partial x_1} - \cdots - 2x_{\lambda} \frac{\partial}{\partial x_{\lambda}}
		    + 2x_{\lambda+1} \frac{\partial}{\partial x_{\lambda+1}}
		    + \cdots + 2x_n \frac{\partial}{\partial x_n}.
	\]
\end{enumerate}
\end{defn}

\section{Existence of a global gradient-like vector field}
We want to show the existence of a global gradient-like vector field for $f \colon M \to \R$. We will need the following lemma, whose proof is based on the proof of Proposition 8.6 in \citep{duPlessisSwann}.
\begin{lem} \label{existenceofstepfunctions}
Let $U$ be a coordinate neighbourhood of $M$ and let $K$ be a compact set in $U$. Then there exists $h \in C^{\infty}(U, \R)$ and sets $K \subseteq V \subseteq L \subseteq U$ satisfying
\begin{enumerate}[label=(\roman*)]
	\item $0 \leq h(p) \leq 1$ for all $p \in U$.
	\item $h(p) = 1$ for all $p \in V$, where $V$ is some open neighbourhood $V$ of $K$.
	\item $h(p) = 0$ for all $p \in U \setminus L$, where $L$ is some compact set $L$ containing $V$.
\end{enumerate}
\end{lem}
\begin{proof}
Since $U$ is open, we have that for every $p \in K$, there exists an $\varepsilon_p > 0$ such that 
\begin{equation}
	B_{3\varepsilon_p}(p) \subseteq U.
\end{equation}
Then
\begin{equation}
	C = \makeset{B_{\varepsilon_p}(p)}{p \in K}
\end{equation}
is an open cover of $K$, and since $K$ is compact, we may choose finitely many such open balls $B_{\varepsilon_1}(p_1), \ldots, B_{\varepsilon_k}(p_k)$ such that
\begin{equation}
	V = \bigcup_{i=1}^k B_{\varepsilon_i}(p_i)
\end{equation}
is a finite open cover of $K$, so $K \subseteq V \subseteq U$. Let
\begin{equation}
	\psi_i(x) = \begin{cases}
		\text{exp} \left[-1 \middle/ \para{(2 \varepsilon_i)^2 - \norm{x - p_i}^2}\right] & \text{ if } x \in B_{\varepsilon_i}(p_i), \\
		0 & \text{ otherwise.}
	\end{cases}
\end{equation}
Note that $\psi_i$ is smooth, with $\psi_i(x) > 0$ for all $x \in B_{\varepsilon_i}(p_i)$, and that
\begin{equation}
	\text{supp}(\psi_i) = \overline{\makeset{x \in U}{\psi_i(x) \ne 0}} \subseteq B_{2 \varepsilon_i}(p_i).
\end{equation}
Now, define the map
\begin{equation}
	\phi_i = \sum_{i=1}^k \psi_i \mathds{1}_{B_{\varepsilon_i}(p_i)},
\end{equation}
which has $\text{supp}(\phi_i) \subseteq V$. Now define
\begin{equation}
	S(x) = \sum_{i=1}^k \phi_i(x),
\end{equation}
and note that $S(x) > 0$ for all $x \in K$. Define the map
\begin{equation}
	\kappa_i(x) = \begin{cases}
		\text{exp} \left[-1 \middle/ \para{\norm{x - p_i}^2 - \varepsilon_i^2}\right] & \text{ if } x \in U \setminus B_{\varepsilon_i}(p_i), \\
		0 & \text{ otherwise.}
	\end{cases}
\end{equation}
Note that $\text{supp}(\kappa_i) = U \setminus B_{\varepsilon_i}(p_i)$. Let $T = \prod_{i=1}^k \kappa_i$. Then $T(x) = 0$ for all $x \in V$, and by construction we have $T(x) + S(x) > 0$ for all $x \in U$. Then the following functions are well-defined:
\begin{equation}
	\varphi_i(x) = \frac{\phi_i(x)}{T(x) + S(x)}
\end{equation}
for all $x \in U$, and are smooth, with $\text{supp}(\varphi_i) \subseteq V$.

Finally, define
\begin{equation}
	h(x) = \sum_{i=1}^k \varphi_i(x)
\end{equation}
on $U$. By construction it is smooth and $h(x) \geq 0$ for all $x \in U$. Note that 
\begin{equation}
	\phi_i(x) \leq S(x) \leq T(x) + S(x),
\end{equation}
so $h(x) \leq 1$ for all $x \in U$. This proves (i). Let $x \in V$, then $T(x) = 0$, so
\begin{equation}
	h(x) = \sum_{i=1}^k \varphi_i(x) = \frac{1}{S(x)} \sum_{i=1}^k \phi_i(x) = \frac{S(x)}{S(x)} = 1,
\end{equation}
which shows (ii). Define
\begin{equation}
	L = \bigcup_{i=1}^k \text{supp}(\phi_i) \subseteq U,
\end{equation}
then $L$ is compact, since it is a finite union of compact sets. By construction we have that $h(x) = 0$ for $x \in U \setminus L$, which shows (iii).
\end{proof}

Now we are ready to prove:
\begin{thm} \label{existenceofglobalgradientlike}
	Suppose that $f \colon M \to \R$ is a Morse function on a compact manifold $M$. Then there exists a gradient-like vector field $X$ on $M$ for $f$.
\end{thm}
\begin{proof}
Since $M$ is a manifold, there exists an open cover
\[
	\mathcal{C} = \makeset{U}{U \text{ is a coordinate neighbourhood of $M$}}
\]
for $M$, that is
\[
	M \subseteq \bigcup_{U \in \mathcal{C}} U.
\]
Now, since $M$ is compact, we may choose a finite collection $U_1, \ldots, U_k$ of elements from $\mathcal{C}$ such that
\[
	M \subseteq \bigcup_{i=1}^k U_i.
\]
Thus $M$ is covered by finitely many coordinate neighbourhoods. For each $U_i$, consider
\begin{align}
	\mathcal{D}_i &= \makeset{\overline{D} \subseteq U_i}{D \cong B_r(p), \, r > 0, \, p \in U_i} \\
	&= \makeset{\mathbf{x}_i(\overline{B_s(q)})}{\overline{B_s(q)} \subseteq \mathbf{x}_i^{-1}(U_i), \, s > 0}, \label{theyrecompact}
\end{align}
the set of $n$-disks contained in each coordinate neighbourhood. By (\ref{theyrecompact}) we see that every disk in $\mathcal{D}_i$ is compact, since compactness is a topological property. Since each $U_i$ is open, it follows that for every point $p \in U_i$ there is atleast one disk in $D \in \mathcal{D}_i$ such that $p \in \text{int}(D)$. Let $\mathcal{D} = \mathcal{D}_1 \cup \cdots \cup \mathcal{D}_k$. It follows that
\[
	M \subseteq \bigcup_{D \in \mathcal{D}} \text{int}(D) %\bigcup_{i=1}^k \para{\bigcup_{D \in \mathcal{D}_i} \text{int}(D)}
\]
is an open cover for $M$. Again, by the compactness, we may then choose finitely many disks $D_1, \ldots, D_l$ from $\mathcal{D}$ such that the union of their interiors still cover $M$. For each $D_i$, we may associate to it which $U_i$ it is contained in. We'll instead write $D_{(i,j)}$ for the $n_i$ disks that are contained in $U_i$. Then let
\[
	K_i = \bigcup_{j=1}^{n_i} D_{(i,j)} \subseteq U_i
\]
for $i = 1, \ldots, k$, which is a compact set. Then $K_1 \cup \cdots \cup K_k$ is a compact cover for $M$.

For each critical point $p_0 \in M$ of $f$ we have that $p_0$ is contained in at least one $U_i$, since they cover $M$. Since $p_0$ is a critical point of $f$, Morse's Lemma gives us that there exists a coordinate neighbourhood $U_0$ with coordinates $(y_1, \ldots, y_n)$ such that $f$ assumes the standard form
\begin{equation} \label{standardformusedfirsttime}
	f = -y_1^2 - y_2^2 - \cdots - y_{\lambda}^2 + y_{\lambda+1}^2 + \cdots + y_n^2 + f(p_0)
\end{equation}
on $U_0$. Let $\mathcal{U}$ denote the collection of these neighbourhoods. Since each critical point is isolated, by Corollary \ref{criticalpointsareisolated}, we may choose each neighbourhood small enough so that they don't intersect each other. Since $M$ is a manifold, we may extend $\mathcal{U}$ to a collection of coordinate neighbourhoods that cover $M$, and then by compactness we may reduce it to a finite cover of $M$. Thus we may assume that $p_0$ belongs to exactly one $U_i$, and we assume from now on that the $U_i$'s were constructed this way.

Let $f_i = f \big\lvert_{U_i}$ for all $i = 1, \ldots, k$ and then let $X_{f_i}$ denote its gradient vector field. Let $x_1, \ldots, x_n$ denote the coordinates of $U_i$, and note that on $U_i$ we have that
\[
	X_{f_i}(f) = X_{f_i}(f_i) = \sum_{j=1}^k \para{\frac{\partial f_i}{\partial x_j}}^2 \geq 0.
\]
In particular, we have that $X_{f_i}(f) > 0$ away from critical points of $f$, thus $X_{f_i}$ satisfies (i) in Definition \ref{gradientlikedefn}. If $U_i$ contains a critical point then we assumed that $f$ takes the standard form (\ref{standardformusedfirsttime}), so $X_{f_i}$ satisfies (ii) in Definition \ref{gradientlikedefn}. Thus $X_{f_i}$ is a gradient-like vector field for $U_i$.

Now we want to globalize. For each pair $(U_i, K_i)$, we have by Lemma \ref{existenceofstepfunctions} that there exists a smooth function
\[
	\tilde{h}_i \colon U_i \to \R
\]
and sets $V_i, L_i$ such that conditions (i)-(iii) in Lemma \ref{existenceofstepfunctions} are satisfied. Then we define $h_i \colon M \to \R$ by
\[
	h_i(p) = \begin{cases}
		\tilde{h}_i(p) & \text{ if } p \in U_i, \\
		0 & \text{ if } p \in M \setminus U_i.
	\end{cases}
\]
This is smooth since $h_i$ and $\tilde{h}_i$ agree on an open set containing the boundary of $U_i$. We shall consider the vector field
\[
	h_i X_{f_i}
\]
as a smooth vector field on $M$, by having it be $0$ outside $U_i$, which also makes sense since $h_i \equiv 0$ outside $U_i$. Finally, we define the vector field
\[
	X = \sum_{i=1}^k h_i X_{f_i}
\]
and show that this is a gradient-like vector field for all of $M$. Let $p \in M$ and assume that $p$ is not a critical point of $f$. Then $p \in K_i \subseteq U_i$, for some $i$, so $h_i(p) = 1$ and since $p$ is not a critical point we have $(X_{f_i})_p(f) > 0$. Hence $X_p(f)$ has the term
\[
	h_i(p) \cdot (X_{f_i})_{p}(f) > 0,
\]
and the rest of the terms satisfy 
\[
	h_j(p) \cdot (X_{f_j})_{p}(f) \geq 0.
\]
Thus $X(f) > 0$ away from critical points, so $X$ satisfies (i) in Definition \ref{gradientlikedefn}. Now assume that $p \in M$ is a critical point of $f$. By assumption, we have that $p$ belongs to exactly one $U_i$, hence
\[
	X_p(f) = h_{i}(p) \cdot (X_{f_i})_{p}(f).
\]
Since $p \in K_j$ for some $j$, then since $p \in U_i$ we must have that $i = j$, so $h_{i}(p) = 1$, hence $X$ and $X_{f_i}$ agree on $p$, thus $X$ satisfies (ii) in Definition \ref{gradientlikedefn}. Thus $X$ is a global gradient-like vector field on $M$ for $f$.

\end{proof}