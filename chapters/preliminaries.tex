\chapter{Introduction}

In this chapter we introduce some notions that we will use throughout the project.

\input{chapters/introduction.tex}

\section{Notation abuse}
\begin{rmk}
We first introduce some ways we will shorten notation. Let $U, V \subseteq \R^n$ be open sets. Consider a smooth map \[
	f \colon V \to \R.
\]
Let
\[
	\mathbf{x}, \mathbf{y} \colon U \to V
\]
be smooth maps. We will refer to these as \textit{coordinate systems} for $f$, such that for a point $p \in V$, we have
\[
	q = \mathbf{x}^{-1}(p) \in U \quad \text{and} \quad r = \mathbf{y}^{-1}(p) \in U,
\]
such that
\[
	f(p) = f \circ \mathbf{x}(q) = f \circ \mathbf{y}(r).
\]
The \textit{change of coordinates} map between these two coordinate systems is given by the diffeomorphism
\[
	\Phi = \mathbf{x^{-1}} \circ \mathbf{y} \colon U \to U.
\]
We shall consider $f$ as a ``function of $\mathbf{x}$'' as the map $f \circ \mathbf{x}$, with $\mathbf{x} = (x_1, \ldots, x_n)$. We will write $\displaystyle \frac{\partial f}{\partial x_i}$ instead of $\displaystyle \frac{\partial (f \circ \mathbf{x})}{\partial x_i}$, and we define evaluation at $p \in V$ as
\[
	\para{\frac{\partial f}{\partial x_i}}(p) := \para{\frac{\partial (f \circ \mathbf{x})}{\partial x_i}}(q).
\]
Denoting $\mathbf{y} = (y_1, \ldots, y_n)$ and $\Phi = (\phi_1, \ldots, \phi_n)$ then we shall write the entries of the Jacobian $J_{\Phi}$ for $\Phi$ as:
\[
	\frac{\partial x_k}{\partial y_i} := \frac{\partial \phi_k}{\partial y_i}.
\]
Thus by using the changes of notation discussed above, we can make sense of the following expression:
\begin{equation} \label{chainrule}
	\frac{\partial f}{\partial y_i}(p) = \sum_{k=1}^{n} \frac{\partial f}{\partial x_k}(p) \frac{\partial x_k}{\partial y_i}(p).
\end{equation}
It is actually the following expression in disguise:
\begin{equation}
	\frac{\partial(f \circ \mathbf{y})}{\partial y_i}(r) = \sum_{k=1}^n \frac{\partial (f \circ \mathbf{x})}{\partial x_k}(q) \frac{\partial \phi_k}{\partial y_i}(r).
\end{equation}
From now on we shall shorten notation in this manner.
\end{rmk}

\section{Vector fields}
Let $M$ be a $n$-manifold embedded in $\R^N$. Let $U \subseteq \R^n$ be an open set with coordinates $(u_1, \ldots, u_n)$ and let $(\mathbf{x}(U), \mathbf{x})$ be a chart containing a point $p \in M$. The situation looks like this:
\begin{equation}
	\R^n \supseteq U \xrightarrow{\quad \mathbf{x} \quad} \mathbf{x}(U) \subseteq M \xrightarrow{\quad f \quad} \R.
\end{equation}
Let $q = \mathbf{x}^{-1}(p) = (q_1, \ldots, q_n) \in U$.

Define the coordinate curves $c_i$, $i = 1, \ldots, n$, in $U$ by
\begin{align*}
	c_i \colon I &\to U \\
	t &\mapsto (q_1, \ldots, q_i + t, \ldots, q_n),
\end{align*}
with $c_i(0) = q$, where $I$ is some open interval containing 0. %Then we may think of $\mathbf{x} \circ c_i$ as the coordinate curve for $x_i$. 
Assume that $x_1, \ldots, x_n$ are the coordinates of $M$. Define
\begin{equation} \label{basisfortangentspace}
	\para{\frac{\partial}{\partial x_i}}_p := \frac{d}{dt}\mathbf{x}(c_i(t)) \bigg\lvert_{t=0} \in \R^N, \quad i = 1, \ldots, n. % \frac{\partial \mathbf{x}}{\partial u_i}(q)
\end{equation}
Then we define the tangent space of $M$ at $p$ by
\begin{equation}
	T_p M = \text{Span}\left[\para{\frac{\partial}{\partial x_1}}_p, \ldots, \para{\frac{\partial}{\partial x_n}}_p\right].
\end{equation}
This is an $n$-dimensional subspace of $\R^N$, since the vectors $c_1'(0), \ldots, c_n'(0)$ are a basis for $\R^n$ and $\mathbf{x}$ is a diffeomorphism, thus the $n$ vectors in (\ref{basisfortangentspace}) are linearly independent. With the convention
\begin{equation}
	\frac{\partial f}{\partial x_i} := \frac{d}{dt}(f \circ \mathbf{x} \circ c_i),
\end{equation}
we note that the notation of (\ref{basisfortangentspace}) is justified, and we may consider $\frac{\partial}{\partial x_i}$ as a differential operator, so that we write:
\begin{equation} \label{tobeconsistentwith}
	\para{\frac{\partial}{\partial x_i}}_p \cdot f := \frac{\partial f}{\partial x_i}(p).
\end{equation}

\begin{rmk} \label{jacobianandcoordinatevectors}
Let $p \in M$. If we let \[ \mathcal{V} = \para{\para{\frac{\partial}{\partial x_1}}_p, \ldots, \para{\frac{\partial}{\partial x_n}}_p} \] and \[ \mathcal{W} = \para{\para{\frac{\partial}{\partial y_1}}_p, \ldots, \para{\frac{\partial}{\partial y_n}}_p} \] denote bases for $T_p M$, where $x_1, \ldots, x_n$ and $y_1, \ldots, y_n$ are coordinate systems of $M$ at $p$, then for $v \in T_p M$ write
\[
	v = \sum_{i=1}^n \alpha_i \para{\frac{\partial}{\partial x_i}}_p = \sum_{i=1}^n \beta_i \para{\frac{\partial}{\partial y_i}}_p,
\]
so that the coordinate vectors in the two bases of $v$ are given
\[
	[v]_{\mathcal{V}} = (\alpha_1, \ldots, \alpha_n)^T,
	\quad \text{and} \quad
	[v]_{\mathcal{W}} = (\beta_1, \ldots, \beta_n)^T.
\]
Then they are related through the Jacobian of the coordinate change $\Phi$:
\[
	[v]_{\mathcal{V}} = J_{\Phi} [v]_{\mathcal{W}}.
\]
This is because of the chain rule, which is given in equation (\ref{chainrule}).
\end{rmk}

\begin{defn}[Directional derivative]
Let $p \in M$ and let $\gamma \colon I \to M$ be a smooth curve, such that $\gamma(0) = p$ and $\gamma'(0) = v = (v_1, \ldots, v_n)$. Then we define the \textit{directional derivative of $f$ at $p$}:
\begin{align*}
	v \cdot f := \frac{d}{dt} f(\gamma(t)) \bigg\lvert_{t=0}
	= \sum_{i=1}^n \frac{\partial f}{\partial x_i}(p) \frac{d \gamma_i}{dt}(0)
	= \sum_{i=1}^n v_i \frac{\partial f}{\partial x_i}(p).
\end{align*}
Note that it is independent of the choice of curve $\gamma$, so it is well-defined. We may think of the above as a vector acting on $f$ from the left by taking the directional derivative of $f$ in the direction $v$. This is consistent with the notation in (\ref{tobeconsistentwith}).
\end{defn}

\begin{defn}[Vector field] \label{defnvectorfield}
Let $\xi_1, \ldots, \xi_n \in C^{\infty}(U, \R)$. Define the differential operator %For every $p \in \mathbf{x}(U)$, define the correspondence
\[
	X = \sum_{i=1}^n \xi_i \frac{\partial}{\partial x_i}.
\]
Then we call $X$ a smooth \textit{vector field} on $\mathbf{x}(U)$. We say that $X$ is a smooth vector field on $M$ if it is a smooth vector field on every coordinate neighbourhood. For each $p \in \mathbf{x}(U)$, $X$ gives us a point
\begin{equation} \label{xpee}
	X_p = \sum_{i=1}^n \xi_i(p) \para{\frac{\partial}{\partial x_i}}_p \in T_{p} M.
\end{equation}
A vector field $X$ acts on a function $f \colon M \to \R$ by
\[
	X(f) = \sum_{i=1}^n \xi_i \frac{\partial f}{\partial x_i}.
\]
Finally, $X$ may act on $f$ evaluated at $p$ by:
\[
	X_p(f) = \sum_{i=1}^n \xi_i(p) \frac{\partial f}{\partial x_i}(p).
\]
\end{defn}

\begin{defn}[Gradient vector field]
By letting $\xi_i = \frac{\partial f}{\partial x_i}$ in Definition \ref{defnvectorfield}, we get a vector field
\[
	X_f = \sum_{i=1}^n \frac{\partial f}{\partial x_i} \frac{\partial}{\partial x_i}.
\]
This is called the \textit{gradient vector field} of $f$. With notation as above, then note that for $g \colon M \to \R$ we have
\[
	(X_f)_{p}(g) = \sum_{i=1}^n \frac{\partial f}{\partial x_i} (p) \frac{\partial g}{\partial x_i} (p).
\]
\end{defn}