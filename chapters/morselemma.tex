\chapter{The Morse Lemma}

\section{Critical points}
\begin{defn}[Critical points and their type]
%Define critical points.
Let $\mathbf{x} = (x_1, \ldots, x_n)$ be a coordinate system for $f \in C^{\infty}(V, \R)$. Then we say that $p \in V$ is a \textit{critical point of $f$ with regards to the coordinate system $\mathbf{x}$}, if
\begin{equation}
	\frac{\partial f}{\partial x_1}(p) = \cdots = \frac{\partial f}{\partial x_n}(p) = 0.
\end{equation}
%Define Hessian.
We define the \textit{Hessian of $f$ with regards to the coordinate system $\mathbf{x}$} as the matrix $H_{\mathbf{x}} f$, with entries given by the maps
\begin{equation}
	[H_{\mathbf{x}} f]_{ij} = \frac{\partial^2 f}{\partial x_i \partial x_j}.
\end{equation}
%Define types of critical points (degenerate, non-degenerate).
Let $p \in V$ be a critical point of $f$. Then if $\det(H_{\mathbf{x}} f) \ne 0$, then we say that $p$ is a \textit{non-denegerate critical point of $f$ with regards to the coordinate system $\mathbf{x}$.} If $\det(H_{\mathbf{x}} f) = 0$ we say that $p$ is \textit{dengenerate} instead.
\end{defn}

\begin{rmk}
It is a well known fact of multivariate calculus that ${\pdderive{f}{x}{y} = \pdderive{f}{y}{x}}$, cf. \citep{duPlessisSwann} Lemma 4.29, hence the Hessian is a symmetric matrix.
\end{rmk}

\begin{lem}
Let $\mathbf{x} = (x_1, \ldots, x_n)$ and $\mathbf{y} = (y_1, \ldots, y_n)$ be coordinate systems for $f \in C^{\infty}(V, \R)$ and let $\Phi = \mathbf{x}^{-1} \circ \mathbf{y}$ denote the change of coordinates map. Let $p \in V$. Then
\begin{enumerate}[label=(\roman*)]
	\item $p$ is a critical point of $f$ with regards to the coordinate system $\mathbf{x}$ if and only if $p$ is a critical point of $f$ with regards to the coordinate system $\mathbf{y}$.
	\item Assume that $p$ is a critical point of $f$. Then
	\begin{equation} \label{hessianeq}
		H_{\mathbf{y}} f(p) = J_{\Phi}^T(p) H_{\mathbf{x}}f(p) J_{\Phi}(p)
	\end{equation}
	holds, where $J_{\Phi}$ is the Jacobian of $\Phi$.
	\item The type of a critical point (non-degenerate, degenerate) is invariant under the change of coordiantes.
\end{enumerate}
\end{lem}


\begin{proof}
Assume that $p$ is a critical point of $f$ with regards to the coordinate system $\mathbf{x}$. Then by the chain rule we have
\begin{equation}
	\frac{\partial f}{\partial y_i}(p) = \sum_{k=1}^{n} \frac{\partial f}{\partial x_k}(p) \frac{\partial x_k}{\partial y_i}(p) = 0
\end{equation}
for all $i$, as $\displaystyle \frac{\partial f}{\partial x_k}(p) = 0$ for all $k$ by assumption, which proves (i). Thus we may drop the ``with regards to the coordinate system $\mathbf{x}$'' part, and simply say that ``$p$ is a critical point of $f$''.

Assume that $p$ is a critical point of $f$. Then
\begin{align}
	[H_{\mathbf{y}} f]_{ij} &= \frac{\partial^2 f}{\partial y_i \partial y_j} \\
	&= \frac{\partial}{\partial y_i} \para{\sum_{k=1}^n \frac{\partial f}{\partial x_k} \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \frac{\partial}{\partial y_i} \para{\frac{\partial f}{\partial x_k} \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \para{\frac{\partial f}{\partial x_k} \cdot \frac{\partial}{\partial y_i}\para{\frac{\partial x_k}{\partial y_j}} + \frac{\partial}{\partial y_i} \para{\frac{\partial f}{\partial x_k}} \cdot \frac{\partial x_k}{\partial y_j}} \\
	&= \sum_{k=1}^n \para{\frac{\partial f}{\partial x_k} \frac{\partial^2 x_k}{\partial y_i \partial y_j}
	   + \frac{\partial x_k}{\partial y_j} \sum_{h=1}^n \para{\frac{\partial^2 f}{\partial x_h \partial x_k} \frac{\partial x_h}{\partial y_i}}}.
\end{align}
By assumption $\frac{\partial f}{\partial x_k}(p) = 0$ for all $k$, so by evaluating the above at $p$ we obtain
\begin{equation} \label{doublesum}
	[H_{\mathbf{y}} f]_{ij}(p)
	= \sum_{k=1}^n \sum_{h=1}^n \frac{\partial x_k}{\partial y_j}(p) \frac{\partial^2 f}{\partial x_h \partial x_k}(p) \frac{\partial x_h}{\partial y_i}(p) = [J_{\Phi}^T H_{\mathbf{x}} f J_{\Phi}]_{ij}(p),
\end{equation}
for all $i, j = 1, \ldots, n$, which proves (ii).

Taking the determinant on both sides of (\ref{hessianeq}) we get
\begin{align}
	\det (H_{\mathbf{y}} f) \Big\lvert_{p} &= \det(J_{\Phi}^T) \det(H_{\mathbf{x}}f) \det(J_{\Phi}) \Big\lvert_{p} \\
	&= \det(J_{\Phi})^2 \det(H_{\mathbf{x}}f) \Big\lvert_{p}.
\end{align}
Since $\Phi$ is a diffeomorphism, we have that $\det(J_{\Phi}(p)) \ne 0$, hence it follows that $\det(H_{\mathbf{x}} f(p)) = 0$ if and only if $\det(H_{\mathbf{y}} f(p)) = 0$, which proves (iii).
\end{proof}

\section{A result from calculus}
\begin{lem} \label{factfromcalculuslem}
Let $\mathbf{x} = (x_1, \ldots, x_n)$ be a coordinate system for $f \in C^{\infty}(V, \R)$, where $V \subseteq \R^n$ is open and contains the origin $\mathbf{0}$, and assume that $f(\mathbf{0}) = 0$. Then there exists functions $g_1, \ldots, g_n \in C^{\infty}(V, \R)$, for some open neighbourhood $V \subseteq U$ of $(0,0)$, such that
\begin{equation} \label{facttfromcalculus}
	f(\mathbf{x}) = \sum_{i=1}^n x_i g_i(\mathbf{x})
\end{equation}
on $V$, and
\begin{equation} \label{facttfromcalculusII}
	\frac{\partial f}{\partial x_i}(\mathbf{0}) = g_i(\mathbf{0})
\end{equation}
for all $i = 1, \ldots, n$.
\end{lem}
\begin{proof}
Assume, without loss of generality, that $f$ is defined on all of $\R^n$. Fix a point $\mathbf{x_0} \in \R^n$ and define the map
\begin{align*}
	m \colon \R &\to \R \\
	t &\mapsto f(t \, \mathbf{x_0}).
\end{align*}
Note that $m(0) = f(\mathbf{0}) = 0$, so by a corollary to the fundamental theorem of calculus (\citep{ETP}, Theorem 8.18) we have that
\begin{equation}
	\int_{0}^{1} \left(\frac{d}{dt} m(t) \right) dt = m(1) - m(0) = m(1) = f(\mathbf{x_0}).
\end{equation}
The chain rule (\citep{ETP}, Theorem 9.14) gives us that
\begin{equation}
	\frac{d}{dt} m(t) = \sum_{i=1}^n (\mathbf{x_0})_i \frac{\partial f}{\partial x_i}(t \, \mathbf{x_0})
\end{equation}
and then by using the fact that integration is linear, we see that
\begin{equation}
	f(\mathbf{x_0}) = \sum_{i=1}^n (\mathbf{x_0})_i \int_{0}^{1} \frac{\partial f}{\partial x_i}(t \, \mathbf{x_0}) \, dt,
\end{equation}
and since $\mathbf{x_0}$ was arbitrary, the above is true on all of $\R^n$, so we get (\ref{facttfromcalculus}), with
\begin{equation}
	g_i(\mathbf{x}) = \int_{0}^{1} \frac{\partial f}{\partial x_i}(t \, \mathbf{x}) \, dt.
\end{equation}
Finally, we note that
\begin{equation}
	g_i(\mathbf{0}) = \int_{0}^{1} \frac{\partial f}{\partial x_i}(\mathbf{0}) \, dt = \frac{\partial f}{\partial x_i}(\mathbf{0}) \int_{0}^{1} \, dt = \frac{\partial f}{\partial x_i}(\mathbf{0}),
\end{equation}
which establishes (\ref{facttfromcalculusII}).
\end{proof}

\section{A result from linear algebra}
\begin{prop} \label{dimensiontoobigthenintersectionisempty}
Let $V$ be a finite dimensional $\F$-vector space. Let $U, W \subseteq V$ be subspaces of $V$. Then
\begin{equation}
	\dim(U) + \dim(W) > \dim(V) \implies U \cap W \ne \curly{0}.
\end{equation}
\end{prop}
\begin{proof}
If $U = W$ then we're done. Assume that $U \ne W$. Let $n, r$ and $s$ denote $\dim(V), \dim(U)$ and $\dim(W)$, respectively. Assume that $r + s > n$. Let $v \in U \cap W$. Then in particular $v \in U$ and $v \in W$, so
\begin{equation} \label{ourstartvector}
	v = \sum_{i=1}^r \alpha_i u_i = \sum_{i=1}^s \beta_i w_i.
\end{equation}
Then
\begin{equation}
	\sum_{i=1}^r \alpha_i u_i - \sum_{i=1}^s \beta_i w_i = 0.
\end{equation}
This may be written
\begin{equation} \label{toobiglinearcombination}
	\alpha_1 u_1 + \cdots + \alpha_r u_r - \beta_1 w_1 - \cdots - \beta_{s} w_s = 0.
\end{equation}
Since $r + s > n$ and the above is a linear combination of $r + s$ vectors, the set $\curly{u_1, \ldots, u_r, w_1, \ldots, w_s}$ must be linearly dependent, so there exists a non-trivial solution to (\ref{toobiglinearcombination}). Let $(\alpha_1', \ldots, \alpha_r', \beta'_1, \ldots, \beta'_s)$ be such a non-trivial solution. Then
\begin{equation}
	v' = \sum_{i=1}^r \alpha'_i u_i = \sum_{i=1}^s \beta'_i w_i
\end{equation}
satisfies that $v' \ne 0$ and $v' \in U \cap W$.
\end{proof}

\section{The Morse Lemma}
We are now ready to state and prove:
\begin{thm}[Morse's Lemma] \label{morselemma}
Let $M \subseteq \R^n$ be a manifold. Let $p_0 \in M$ be a non-degenerate critical point of $f \in C^{\infty}(M, \R)$. Then we may choose a coordiante system $\mathbf{y} = (y_1, \ldots, y_n)$, with $y_i(p_0) = 0$ for all $i$, about $p_0$ such that:
\begin{equation} \label{standardmorseform}
	f(\mathbf{y}) = -y_1^2 - y_2^2 - \cdots - y_{\lambda}^2 + y_{\lambda+1}^2 + \cdots + y_n^2 + f(p_0).
\end{equation}
The number $0 \leq \lambda \leq n$ depends only on $f$ and not on the chosen coordinate system.
\end{thm}
\begin{proof}
Choose a local coordinate system $\mathbf{x} = (x_1, \ldots, x_n)$ near $p_0$. We may assume that $p_0$ is the origin $\mathbf{0}$, since we could just apply a translation $p \mapsto p - p_0$ to $M$ and bring $p_0$ to the origin. We may also assume that $f(p_0) = 0$, since if $f(p_0) \ne 0$, then we could just look at $f_1(\mathbf{x}) = f(\mathbf{x}) - f(p_0)$ instead, which has the property $f_1(p_0) = 0$.

With the assumptions above in place the conditions of Lemma \ref{factfromcalculuslem} are met, so we may write
\begin{equation}
	f(\mathbf{x}) = \sum_{i=1}^n x_i g_i(\mathbf{x}),
\end{equation}
with
\begin{equation}
	g_i(\mathbf{0}) = \frac{\partial f}{\partial x_i}(\mathbf{0}).
\end{equation}
Since $p_0$ is the origin and a critical point, the above equality gives us that $g_i(\mathbf{0}) = 0$ for all $i$, hence we may apply Lemma \ref{factfromcalculuslem} once more on the $g_i$'s, obtaining functions $h_{ij}$ such that
\begin{equation} \label{firstquadraticform}
	f(\mathbf{x}) = \sum_{i=1}^n x_i \para{\sum_{j=1}^n x_j h_{ij}(\mathbf{x})} = \sum_{i,j=1}^n x_i x_j h_{ij}(\mathbf{x}).
\end{equation}
Let $H_{ij} = \tfrac{1}{2} \para{h_{ij} + h_{ji}}$, then
\begin{equation}
	f(\mathbf{x}) = \sum_{i,j=1}^n x_i x_j H_{ij}(\mathbf{x})
\end{equation}
is a quadratic form with $H_{ij} = H_{ji}$. Note that
\begin{equation} \label{coefsarepartials}
	\frac{\partial^2 f}{\partial x_i \partial x_j}(\mathbf{0}) = h_{ij}(\mathbf{0}) + h_{ji}(\mathbf{0}) = 2 H_{ij}(\mathbf{0}),
\end{equation}
since after differentiating (\ref{firstquadraticform}) twice, first with regards to $x_j$, then with regards to $x_i$, the only terms that aren't multiplied by any $x_i$ are $h_{ij}(\mathbf{0})$ and $h_{ji}(\mathbf{0})$.

Suppose by induction that we have found a coordinate system $\mathbf{z} = (z_1, \ldots, z_{n})$ such that
\begin{equation} \label{inductionassumption}
	f = \varepsilon_1 z_1^2 + \cdots + \varepsilon_{r-1} z_{r-1}^2 + \sum_{i,j=r}^n z_i z_j H_{ij}(\mathbf{z}),
\end{equation}
where $\varepsilon_i \in \curly{\pm 1}$ and $H_{ij}$ is symmetric. Now we show that we may assume that $H_{rr} \ne 0$ on some neighbourhood about the origin $\mathbf{0}$. Note that
\begin{equation}
	H_{\mathbf{z}} f(\mathbf{0}) = \begin{pmatrix}
		2 \varepsilon_1 & 0 & \cdots & 0 & 0 \\
		0 & 2 \varepsilon_2 & \cdots & 0 & 0 \\
		\vdots & \vdots & \ddots & \vdots & \vdots \\
		0 & 0 & \cdots & 2 \varepsilon_{r-1} & 0 \\
		0 & 0 & \cdots & 0 & \mathbf{H}
	\end{pmatrix},
\end{equation}
where $\mathbf{H}$ denotes the $(n-r+1) \times (n-r+1)$ matrix with $(\mathbf{H})_{ij} = H_{ij}$. Let $\Phi$ denote the change of coordinates between $\mathbf{x}$ and $\mathbf{z}$, then
\begin{align}
	(\varepsilon_1 \cdots \varepsilon_{r-1}) 2^{r-1} \det(\mathbf{H}) &= \det(H_{\mathbf{z}} f(\mathbf{0})) \\
	&= \det(J_{\Phi}(\mathbf{0}))^2 \det(H_{\mathbf{x}} f(\mathbf{0})) \ne 0,
\end{align}
so $\det(\mathbf{H}) \ne 0$, hence $\mathbf{H}$ is invertible. Hence $N(\mathbf{H}) = \curly{0}$, so it does not have $0$ as an eigenvalue, and since $\mathbf{H}$ is symmetric, it is diagonalizable, so there exists an orthogonal matrix $Q$, such that $D = Q^T \mathbf{H} Q$, where $D$ is a diagonal matrix with the non-zero eigenvalues of $\mathbf{H}$ in the diagonal. Define the matrix
\begin{equation}
\tilde{Q} =
\left(
\begin{array}{c|c}
I & 0 \\
\hline
0 & Q
\end{array}
\right) \in \text{Mat}_n(\R).
\end{equation}
Then through a linear change of coordinates with $\tilde{Q}$ as the Jacobian, we may assume that $H_{rr}(\mathbf{0}) \ne 0$, and since $H_{rr}$ is continuous, we have that $H_{rr} \ne 0$ on some neighbourhood about $\mathbf{0}$.

Now, denoting the last term in (\ref{inductionassumption}) by $R$, we may write
\begin{align}
	R &= H_{rr} \para{\sum_{i,j=r}^n z_i z_j \frac{H_{ij}}{H_{rr}}} \\
	&= H_{rr} \para{z_r^2 + 2 z_r \underbrace{\sum_{j=r+1}^n z_j \frac{H_{rj}}{H_{rr}}}_{K} + \sum_{i,j=r+1}^n z_i z_j \frac{H_{ij}}{H_{rr}}} \\
	&= H_{rr} \para{z_r^2 + 2 z_r K + K^2 - K^2 + \sum_{i,j=r+1}^n z_i z_j \frac{H_{ij}}{H_{rr}}} \\
	&= H_{rr} \para{(z_r + K)^2 - K^2 + \sum_{i,j=r+1}^n z_i z_j \frac{H_{ij}}{H_{rr}}} \\
	&= H_{rr} \cdot (z_r + K)^2 + \sum_{i,j=r+1}^n z_i z_j \para{H_{ij} - \frac{H_{ri}H_{rj}}{H_{rr}}}. \label{lasthrr}
\end{align}
This suggests we define a new coordinate system $\mathbf{y} = (y_1, \ldots, y_n)$ by
\begin{align}
	y_i &= z_i, \quad \text{ if } i \ne r, \\
	y_r &= \sqrt{\abs{H_{rr}}} \para{z_r + K} = \sqrt{\abs{H_{rr}}} \para{z_r + \sum_{j=r+1}^n z_j \frac{H_{rj}}{H_{rr}}}.
\end{align}
The Jacobian $J$ of the coordinate change from $\mathbf{y}$ to $\mathbf{z}$ is the identity matrix, but with the $r$th row equal to the partial derivatives of $y_r$. Thus, developing the determiant of $J$ from the last column, we see that
\begin{equation}
	\det(J) = \frac{\partial y_r}{\partial z_r}(\mathbf{0}) = \sqrt{\abs{H_{rr}(\mathbf{0})}} \ne 0.
\end{equation}
The inverse function theorem then gives that this is a well-defined coordinate system.

Thus we may write
\begin{equation}
	f = \varepsilon_1 y_1^2 + \cdots + \varepsilon_{r-1} y_{r-1}^2 + \varepsilon_r y_{r}^2 + \sum_{i,j=r+1}^n y_i y_j W_{ij},
\end{equation}
where $\varepsilon_r$ is the sign of $H_{rr}$ and from (\ref{lasthrr}) we see that
\begin{equation}
	W_{ij} = H_{ij} - \frac{H_{ri} H_{rj}}{H_{rr}}.
\end{equation}
Note that $W_{ij}$ is symmetric since $H_{ij}$ is symmetric. This completes the induction so $f$ may be written as
\begin{equation}
	f = \varepsilon_1 y_1^2 + \cdots + \varepsilon_n y_n^2,
\end{equation}
with $\varepsilon_i \in \curly{\pm 1}$. Let $\lambda$ denote the number of $y_i$'s such that it has a negative coefficient. Then, by renaming variables if necessary, we may write
\begin{equation}
	f = -y_1^2 - \cdots - y_{\lambda}^2 + y_{\lambda+1}^2 + \cdots + y_n^2 + f(0).
\end{equation}
This proves that $f$ may be written in the form (\ref{standardmorseform}).

\begin{comment}
	\begin{pmatrix}
		-2 &        &    &   &        &   \\
		   & \ddots &    &   &        &   \\
		   &        & -2 &   &        &   \\
		   &        &    & 2 &        &   \\
		   &        &    &   & \ddots &   \\
		   &        &    &   &        & 2
	\end{pmatrix}
\end{comment}

Now we show that $\lambda$ is unique. Let $0 \leq \mu \leq n$, with $\mu \ne \lambda$, and assume that we have another coordiante system $\mathbf{z} = (z_1, \ldots, z_n)$ such that
\begin{equation}
	f = -z_1^2 - \cdots - z_{\mu}^2 + z_{\mu+1}^2 + \cdots + z_n^2 + f(0),
\end{equation}
with $\Phi = \mathbf{y}^{-1} \circ \mathbf{z}$ being the change of coordinates between $\mathbf{y}$ and $\mathbf{z}$. Note that
\begin{equation}
	[H_{\mathbf{y}} f(\mathbf{0})]_{ij} = \begin{cases}
		0 & \text{ if } i \ne j, \\
		-2 & \text{ if } i = j \leq \lambda, \\
		2 & \text{ if } i = j > \lambda.
	\end{cases}
\end{equation}
Let $T_{0}M$ denote the tangent space at $\mathbf{0}$ for $M$. This has bases
\begin{equation} \label{basisOne}
	\mathcal{Y} = \para{\para{\frac{\partial}{\partial y_1}}_0, \, \cdots, \, \para{\frac{\partial}{\partial y_n}}_0}
\end{equation}
and
\begin{equation} \label{basisTwo}
	\mathcal{Z} = \para{\para{\frac{\partial}{\partial z_1}}_0, \, \cdots, \, \para{\frac{\partial}{\partial zy_n}}_0}.
\end{equation}
Define the quadratic form evaluation $E$ by
\begin{align*}
	E \colon T_{0}M &\to \R \\
	v &\mapsto [v]_{\mathcal{Y}}^T \, H_{\mathbf{y}} f(\mathbf{0}) \, [v]_{\mathcal{Y}}.
\end{align*}
Let $v \in T_{0}M$. Then note that $[v]_{\mathcal{Y}} = J_{\Phi}(0) [v]_{\mathcal{Z}}$ by Remark \ref{jacobianandcoordinatevectors}, so that
\begin{align}
	[v]_{\mathcal{Z}}^T \, H_{\mathbf{z}} f(\mathbf{0}) \, [v]_{\mathcal{Z}}
	&= [v]_{\mathcal{Z}}^T \, (J_{\Phi}^T H_{\mathbf{y}} f(\mathbf{0}) J_{\Phi}) \, [v]_{\mathcal{Z}} \\
	&= (J_{\Phi} [v]_{\mathcal{Z}})^T H_{\mathbf{y}} f(\mathbf{0}) (J_{\Phi} [v]_{\mathcal{Z}}) \\
	&= [v]_{\mathcal{Y}}^T \, H_{\mathbf{y}} f(\mathbf{0}) \, [v]_{\mathcal{Y}},
\end{align}
so $E$ is independent of the choice of basis and coordinate system. Note that for
\begin{equation}
	v = \sum_{i=1}^n \alpha_i \para{\frac{\partial}{\partial y_i}}_0 = \sum_{i=1}^n \beta_i \para{\frac{\partial}{\partial z_i}}_0
\end{equation}
we have
\begin{align}
	E(v) &= -\alpha_1^2 - \cdots - \alpha_{\lambda}^2 + \alpha_{\lambda+1}^2 + \cdots + \alpha_n^2 \label{easy1} \\
	&= -\beta_1^2 - \cdots - \beta_{\mu}^2 + \beta_{\mu+1}^2 + \cdots + \beta_n^2. \label{easy2}
\end{align}
Define two subspaces
\begin{align}
	S^{+} &= \left\{\sum_{i=1}^n \alpha_i \para{\frac{\partial}{\partial y_i}}_0 \,\, \Bigg\lvert \,\, \alpha_1 = \cdots = \alpha_{\lambda} = 0\right\} \subseteq T_{0}M, \\
	S^{-} &= \left\{\sum_{i=1}^n \beta_i \para{\frac{\partial}{\partial z_i}}_0 \,\, \Bigg\lvert \,\,  \beta_{\mu+1} = \cdots = \beta_{n} = 0\right\} \subseteq T_{0}M.
\end{align}
Then from (\ref{easy1}) and (\ref{easy2}) we immediately get that $E(u) \geq 0$ for all $u \in S^{+}$ and that $E(u) < 0$ for all $u \in S^{-} \setminus \curly{0}$. Since $\mu \ne \lambda$, we may assume that $\mu > \lambda$. Then
\begin{equation}
	\dim(S^{+}) + \dim(S^{-}) = (n - \lambda) + \mu > (n - \lambda) + \lambda = n = \dim(T_{0}M).
\end{equation}
Then Proposition \ref{dimensiontoobigthenintersectionisempty} gives us that $S^{+} \cap S^{-} \ne \curly{0}$. Let $u \in (S^{+} \cap S^{-}) \setminus \curly{0}$ then $E(u) \geq 0$ and $E(u) < 0$, a contradiction. A similar proof is given if $\mu < \lambda$. Thus we conclude that $\lambda$ is unique.
\end{proof}

\begin{defn}[Index of a critical point]
For a critical point $p$ of $f \colon M \to \R$, the number $\lambda$ in the standard form (\ref{standardmorseform}) is called the \textit{index of $p$}, and it is well-defined by Theorem \ref{morselemma}.
\end{defn}

\begin{defn}[Morse function]
A function $f \colon M \to \R$ is called a \textit{Morse function} if every critical point of $f$ is non-degenerate.
\end{defn}

\section{Corollaries}

\begin{cor} \label{criticalpointsareisolated}
The critical points of a Morse function $f \colon M \to \R$ are isolated.
\end{cor}
\begin{proof}
Let $p_0 \in M$ be a critical point of $f$. Then by Theorem \ref{morselemma} there exists a coordinate system $x_1, \ldots, x_n$ such that we may write
\[
	f = -x_1^2 - \cdots - x_{\lambda}^2 + x_{\lambda+1}^2 + \cdots + x_n^2 + f(p_0)
\]
in a coordinate neighbourhood $U$ containing $p_0$. Then
\[
	\frac{\partial f}{\partial x_i} = \begin{cases}
		-2 x_i & \text{ if } i \leq \lambda \\
		2 x_i & \text{ if } i > \lambda
	\end{cases}
\]
in $U$. This only vanishes when $x_1 = \cdots = x_n = 0$, that is at $p_0$. Hence $p_0$ is the only critical point in the open set $U$, hence it is isolated from the other critical points of $f$.
\end{proof}

\begin{cor} \label{morsefuncfinitelymanycriticals}
A Morse function on a compact manifold $M$ has only finitely many critical points.
\end{cor}
\begin{proof}
Assume that $f$ has at least one critical point. For every critical point $p_i \in M$ of $f$, let $V_i$ be the open neighbourhood of $p_i$ that does not contain any other critical points of $f$ than $p_i$. Hence $p_i \not\in V_j$ for $i \ne j$. Let $I$ be the index set for the $V_i$'s. Let $V = \bigcup_{i \in I} V_i$. Then
\[
	C = (M \setminus V) \cup V
\]
is an open cover for $M$, and since $M$ is compact, we may choose finitely many indices from $I$, by renaming indices if neccessary, such that
\[
	C' = (M \setminus V) \cup \bigcup_{i=1}^k V_i,
\]
such that $C'$ is a finite open cover for $M$. Let $p \in M$ be a critical point of $f$, then $p \in C'$, and $p \not\in M \setminus V$. This means that there exists an index $i$ such that $p \in V_i$, and by construction no other critical points are in $V_i$. It follows that $f$ has exactly $k$ critical points.
\end{proof}