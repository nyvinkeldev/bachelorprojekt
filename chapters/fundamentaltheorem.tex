\chapter{A fundamental theorem of Morse Theory}

\section{Integral curves}
\begin{defn}[Integral curve]
Let $\gamma \colon I \to M$ be a curve and $X$ be a smooth vector field. We say that $\gamma$ is an \textit{integral curve of $X$}, if for every $t_0 \in I$, we have that
\[
	X_p = \frac{d \gamma}{dt}(t_0)
\]
where $p = \gamma(t_0) \in M$. From now on we shall write $X_{\gamma(t)}$.
\end{defn}

We will need the following differential equations result. This is Theorem D.1 from \citep{lee}, p. 664:
\begin{thm} \label{diffeqresult}
Suppose $U \subseteq \R^n$ is open, and $Z \colon U \to \R^n$ is a smooth vector-valued function. Consider the initial value problem
\begin{align}
	\frac{d}{dt}\gamma(t) &= Z(\gamma(t)), \label{ivp1} \\
	\gamma(t_0) &= \eta, \label{ivp2}
\end{align}
for arbitrary $t_0 \in \R$ and $\eta \in U$. Then we have:
\begin{enumerate}[label=(\roman*)]
	\item Existence: For any $t_0 \in \R$ and $x_0 \in U$ there exist an open interval $J_0$ containing $t_0$ and an open subset $U_0 \subseteq U_0$ containing $x_0$ such that for each $\eta \in U_0$ there is a $C^1$ map $\gamma \colon J_0 \to U$ that solves (\ref{ivp1})-(\ref{ivp2}).
	\item Uniqueness: Any two differentiable solutions to (\ref{ivp1})-(\ref{ivp2}) agree on their common domain.
	\item Smoothness: Let $J_0$ and $U_0$ be as in (i), and let $\theta \colon J_0 \times U_0 \to U$ be the map defined by $\theta(t, x) = \gamma(t)$, where $\gamma \colon J_0 \to U$ is the unique solution to (\ref{ivp1}) with initial condition $\gamma(t_0) = x$. Then $\theta$ is smooth.
\end{enumerate}
\end{thm}

\begin{rmk}
Point (iii) of Theorem \ref{diffeqresult} implies that the $C^1$ map $\gamma$ in point (i) can be assumed to be a $C^{\infty}$ map.

Point (ii) means that if $(J_1, \gamma_1)$ and $(J_2, \gamma_2)$ are differentiable solutions to (\ref{ivp1}), then if for some $t_0 \in J_1 \cap J_2$ we have that $\gamma_1(t_0) = \gamma_2(t_0)$, then $\gamma_1 = \gamma_2$ on $J_1 \cap J_2$.
\end{rmk}

\section{A fundamental theorem of Morse Theory}
\begin{defn}
Let $f \colon M \to \R$ be a Morse function on a manifold $M$. Then we define the sub-manifolds:
\begin{equation*}
	M_{[a,b]} = f^{-1} [a, b] \subseteq M
\end{equation*}
and
\begin{equation*}
	M_{c} = f^{-1} (-\infty, c] \subseteq M
\end{equation*}
for all $a, b, c \in \R$.
\end{defn}

The following definition is based on \citep{hatcher}, p. 2:
\begin{defn}[Deformation retract] \label{deforetractdefn}
Let $X$ be a topological space and let $A$ be a subspace of $X$. We say that $A$ is a \textit{deformation retract} of $X$, if there exists a family of maps $\makeset{f_t \colon X \to X}{t \in [0,1]}$ satisfying
\begin{enumerate}[label=(\roman*)]
	\item $f_0 = \text{Id}_X$.
	\item $f_1(X) = A$.
	\item $f_t\big\lvert{A} = \text{Id}_A$ for all $t \in [0,1]$.
	\item The map $(x, t) \mapsto f_{t}(x)$ is continuous.
\end{enumerate}
\end{defn}

This proof is based on \citep{milnor}.
\begin{thm} \label{resulttwo}
Let $M$ be a compact manifold. Let $[a, b]$ be a real interval. If $f$ has no critical values in the interval $[a,b]$, then
\begin{enumerate}[label=(\roman*)]
	\item $M_a$ and $M_b$ are diffeomorphic.
	\item $M_a$ is a deformation retract of $M_b$.
\end{enumerate}
\end{thm}
\begin{proof}
Let $X$ be a gradient-like vector field for $M$, which exists by Theorem \ref{existenceofglobalgradientlike}. Let $C$ denote the set of critical values of $f$, and note that $C$ is finite, by Corollary \ref{morsefuncfinitelymanycriticals}. Then set
\begin{align}
	c_L &= \max\makeset{c \in C}{c \leq a}, \\
	c_R &= \min\makeset{c \in C}{c \geq b}.
\end{align}
Note that $c_L < a$ and $c_R > b$, since $[a,b]$ contains no critical values of $f$. Let
\begin{equation}
	\delta = \tfrac{1}{2} \min\curly{a - c_L, c_R - b} > 0.
\end{equation}
Then $[a-\delta,b+\delta]$ contains no critical values of $f$, and the set $K = M_{[a-\delta,b+\delta]}$ is a compact neighbourhood of $M_{[a,b]}$, since a closed subset of a compact space is compact. Let $\rho \colon M \to \R$ be a smooth map which satisfies that
\begin{align}
	\rho\big\lvert_{K}(q) &= \frac{\omega(q)}{X_q(f)}, \\
	\rho\big\lvert_{M \setminus K}(q) &= 0,
\end{align}
where $\omega \colon M \to \R$ is a smooth map which satisfies $\omega \equiv 1$ on $M_{[a,b]}$ and $0 \leq \omega \leq 1$ on $K$, and $\omega \equiv 0$ on $M \setminus K$. The maps $\rho$ and $\omega$ may be constructed using bump functions, like we did in the proof of Lemma \ref{existenceofstepfunctions}. Then we define the smooth vector field
\begin{equation}
	Y = \rho X
\end{equation}
on $M$.

Now we want to define a family of diffeomorphisms of $M$
\[
	\varphi_t \colon M \to M
\]
such that the set
\[
	\makeset{\varphi_t}{t \in \R} \subseteq C^{\infty}(M,M)
\]
becomes an abelian group with composition of maps $\circ$ as the group operation, such that the following properties are satisfied:
\begin{enumerate}
	\item For every $q \in M$, then the curve $t \mapsto \varphi_t(q)$ is a smooth integral curve of $Y$ satisfying $\varphi_0(q) = q$.
	\item $\varphi_{s+t} = \varphi_{s} \circ \varphi_{t}$ for all $s, t \in \R$.
	\item $\varphi_{-t} = \varphi_{t}^{-1}$ for all $t \in \R$.
\end{enumerate}
Let $q \in M$. If $q \not\in K$ then let $\varphi_{t}(q) = q$ for all $t \in \R$. Then
\[
	\frac{d}{dt} \varphi_t(q) = 0 = Y_q = Y_{\varphi_{t}(q)}
\]
and $\varphi_0(q) = q$. Now assume that $q \in K$. There exists a local coordinate neighbourhood $U \cong \R^n$ of $q$ with coordinates $x_1, \ldots, x_n$. Then on $U$ we may interpret the vector field as
\[
	Y_p = \sum_{i=1}^n \xi_i(p) e_i
\]
where $(e_1, \ldots, e_n)$ is the standard basis for $\R^n$. Then the map 
\[
	p \mapsto Y_p
\]
is a smooth $U \to \R^n$ map defined on an open set $U$. By Theorem \ref{diffeqresult} (i) we then get that there exists an open set $U_0 \subseteq U$ containing $q$ and an open interval $J_0 = (-\varepsilon_0, \varepsilon_0)$ (The proof of Theorem \ref{diffeqresult} gives that $J_0$ takes this form) such that for any $\eta \in U_0$ there exists a smooth curve $\gamma \colon J_0 \to U$ solving the initial value problem
\begin{align}
	\frac{d}{dt} \gamma(t) &= Y_{\gamma(t)}, \\
	\gamma(0) &= \eta.
\end{align}
Since $q$ was arbitrary, we see that we may find such $U_0$'s for every point in $K$. Cover $K$ with such open sets, then since $K$ is compact, we get that we may choose finitely many, let them be given by:
\[
	(V_i, J_i = (-\varepsilon_i, \varepsilon_i)), \quad \text{ for } i = 1, \ldots, k.
\]
Then let
\[
	\varepsilon = \min\curly{\varepsilon_1, \ldots, \varepsilon_k}.
\]
Then for every point $p \in K$ there exists an integral curve $\gamma_p \colon (-\varepsilon, \varepsilon) \to V_j$ of $Y$ such that $\gamma_p(0) = p$ for $\abs{t} < \varepsilon$. We then define
\begin{equation} \label{defnasintcurve}
	\varphi_t(q) = \gamma_q(t)
\end{equation}
for $\abs{t} < \varepsilon$. Note that by definition $\varphi_0 = \text{Id}_M$. Define
\[
	\Sigma_s = \makeset{t \in \R}{\max\curly{\abs{t}, \abs{s}, \abs{s+t}} < \varepsilon}.
\]
Let $s \in \R$ with $\abs{s} < \varepsilon$. Then we want to show that
\begin{equation} \label{welldefinedaddition}
	\varphi_{t+s}(q) = \varphi_{t}(\varphi_{s}(q))
\end{equation}
for all $t \in \Sigma_s$. Let $p = \gamma_q(s)$. Define two $\Sigma_s \to M$ maps
\begin{align*}
	t &\xmapsto{\quad\xi\quad} \varphi_{s+t}(q) = \gamma_q(s + t), \\
	t &\xmapsto{\quad\eta\quad} \varphi_{t}(\varphi_{s}(q)) = \gamma_{p}(t).
\end{align*}
We have that
\[
	\frac{d \xi}{dt}(t) = \frac{d}{dt}\para{\gamma_q(s + t)} = Y_{\gamma_q(s+t)} = Y_{\xi(t)}
\]
and
\[
	\frac{d \eta}{dt}(t) = \frac{d}{dt}\para{\gamma_p(t)} = Y_{\gamma_p(t)} = Y_{\eta(t)}.
\]
We also have that
\[
	\eta(0) = \gamma_{p}(0) = p = \gamma_q(s) = \xi(0).
\]
Thus $\xi$ and $\eta$ solve the same initial value problem, so Theorem \ref{diffeqresult} (ii) then gives us that $\eta = \xi$ on their common domain $\Sigma_s$, hence we have proved (\ref{welldefinedaddition}). 

Now we define $\varphi_t(q)$ for all $\abs{t} \geq \varepsilon$. We may write $t$ as
\[
	t = d \para{\tfrac{\varepsilon}{2}} + r,
\]
where $d \in \Z \setminus \curly{0}$ and $\abs{r} < \tfrac{\varepsilon}{2}$. Then if $d > 0$ set
\[
	\varphi_t(q) = (\underbrace{\varphi_{\varepsilon/2} \circ \cdots \circ \varphi_{\varepsilon/2}}_{d \text{ times}} \circ \, \varphi_{r})(q)
\]
and if $d < 0$ set
\[
	\varphi_t(q) = (\underbrace{\varphi_{-\varepsilon/2} \circ \cdots \circ \varphi_{-\varepsilon/2}}_{-d \text{ times}} \circ \, \varphi_{r})(q).
\]
Every composition is well-defined by (\ref{welldefinedaddition}) since $\abs{\tfrac{\varepsilon}{2}}, \abs{r} < \varepsilon$. This establishes property 1 and 2. Property 3 follows by 1 and 2 since
\[
	\varphi_t \circ \varphi_{-t} = \varphi_{t+(-t)} = \varphi_{0} = \text{Id}_M
\]
for all $t \in \R$. Note that the map $(t, q) \mapsto \varphi_t(q)$ is smooth by Theorem \ref{diffeqresult} (iii), hence in particular the maps $\varphi_t$ are smooth for every $t$. By property 3 they are all invertible, hence bijective, so they are all $M \to M$ diffeomorphisms. Thus we have established the existence of a group of diffeomorphisms of $M$ satisfying properties 1--3.

Now we show that the $M \to M$ diffeomorphism $\varphi_{b-a}$ restricted to $M_a$ is the $M_a \to M_b$ diffeomorphism we are looking for. Note that for every $q \in K$, we have that
\begin{align}
	\frac{d}{dt} f(\varphi_t(q)) &= \frac{d}{dt} \varphi_{t}(q) \cdot f \label{sideone} \\
	&= Y_{\varphi_{t}(q)}(f) \\
	&= \frac{\omega(\varphi_{t}(q))}{X_{\varphi_{t}(q)}(f)} X_{\varphi_{t}(q)}(f) \\
	&= \omega(\varphi_{t}(q)) \\
	&\in [0,1]. \label{sidetwo}
\end{align}
If $q \not\in K$, we have that $\frac{d}{dt} f(\varphi_t(q)) = 0$. Then the left handside of (\ref{sideone}) and (\ref{sidetwo}) gives that
\begin{equation}
	\begin{cases}
		f(q) \leq f(\varphi_t(q)) \leq f(q) + t & \text{ if } t \geq 0 \\
		f(q) + t \leq f(\varphi_t(q)) \leq f(q) & \text{ if } t \leq 0
	\end{cases}
\end{equation}
for all $q \in M$ and
\begin{equation} \label{inab}
	f(\varphi_t(q)) = f(q) + t
\end{equation}
if $\varphi_t(q) \in M_{[a,b]}$. Let $p \in M_a$. Then
\begin{equation}
	f(\varphi_{b-a}(p)) \leq f(p) + (b - a) \leq a + b - a = b.
\end{equation}
Thus $\varphi_{b-a}(M_a) \subseteq M_b$. Now let $p \in M_b$ such that $f(p) = b$. Then
\begin{equation}
	b = f(p) \geq f(\varphi_{a-b}(p)) \geq f(p) + (a - b) = b + a - b = a.
\end{equation}
So $f(\varphi_{a-b}(p)) \in M_{[a,b]}$, which gives us that
\begin{equation}
	f(\varphi_{a-b}(p)) = f(p) + (a - b) = b + a - b = a.
\end{equation}
Now let $q \in M_b$ be arbitrary. If $f(q) \leq a$, then
\begin{equation}
	f(\varphi_{a-b}(q)) \leq f(q) \leq a.
\end{equation}
If $a < f(q) \leq b$, then we note that since 
\begin{align}
	f(\varphi_{a-b}(p)) &= a, \\
	f(\varphi_{0}(p)) &= f(p) = b,
\end{align}
the intermediate value theorem applied to the continuous $\R \to \R$ map 
\begin{equation} \label{coolcontmap}
s \mapsto f(\varphi_{s}(p))
\end{equation}
then gives us that there exists a $t \in [a-b, 0]$ such that
\begin{equation}
	f(\varphi_{t}(p)) = f(q).
\end{equation}
Then
\begin{equation}
	f(q) = f(\varphi_t(p)) \leq f(\varphi_0(p)) = f(p)
\end{equation}
and then using that the map in (\ref{coolcontmap}) is monotonically increasing, we get
\begin{equation}
	f(\varphi_{a-b}(q)) = f(\varphi_{a-b+t}(p)) \leq f(\varphi_{a-b}(p)) = a,
\end{equation}
so $\varphi_{a-b}(M_b) \subseteq M_a$. Now let $p \in M_b$ and set $p' = \varphi_{a-b}(p)$. Then by what we just showed $p' \in M_a$, and then
\begin{equation}
	\varphi_{b-a}(p') = \varphi_{b-a}(\varphi_{a-b}(p)) = p \in \varphi_{b-a}(M_a).
\end{equation}
Thus $M_b \subseteq \varphi_{b-a}(M_a)$. Hence the map
\[
	\varphi_{b-a}\big\lvert_{M_a} \colon M_a \to M_b
\]
carries $M_a$ diffeomorphically into $M_b$, which shows (i).

Now define a family of $M_b \to M_b$ maps:
\[
	r_t(q) = \begin{cases}
		q & \text{ if } f(q) < a \\
		\varphi_{t(a-f(q))}(q) & \text{ if } a \leq f(q) \leq b
	\end{cases}
\]
for all $t \in [0,1]$. By definition we have that $r_0(q) = q$ for all $q \in M_b$, hence $r_0 = \text{Id}_{M_b}$. Also note that $r_t\big\lvert_{M_a} = \text{Id}_{M_a}$ for all $t$. Now let $q \in M_b$. If $f(q) < a$, then $r_1(q) \in M_a$. If $f(q) \geq a$, then
\begin{equation}
	f(r_1(q)) = f(\varphi_{a-f(q)}(q)) = f(q) + (a - f(q)) = a,
\end{equation}
so $r_1(q) \in M_a$. Thus $f_1(M_b) = M_a$. Now note that the map $(t, q) \mapsto t(a-f(q))$ is continuous since $f$ is smooth, hence the map $(t, q) \mapsto r_t(q)$ is continuous since the map $(t, q) \mapsto \varphi_t(q)$ is smooth. Thus all conditions of Definition \ref{deforetractdefn} are satisfied, so $M_a$ is a deformation retract of $M_b$, which shows (ii).
\end{proof}